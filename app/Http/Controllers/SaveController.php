<?php



namespace App\Http\Controllers;



use Illuminate\Http\Request;

use App\Http\Controllers\Controller;



use Illuminate\Support\Facades\DB;

use View;

use Session;

use Redirect;

use Crypt;

use Mail;

use Artisan;

use App\Mail\ContactMail;

use App\Mail\NewsletterAdminMail;

use App\Mail\NewsletterMail;

use App\Mail\ContestMail;



class SaveController extends Controller

{
	
	
	
	public function save_contest(Request $request)

    {
		
// 		echo 'asdadas';
// 		exit;
		
		$ad_email = "soorya@7harvestme.com";
// 		$ad_email =["nidheesh@netstager.com", "giri@crabiz.com"];
		$contestname = $request->contest_name;
		
		$contestphone = $request->contest_phone;
		
		$contestemail = $request->contest_email;
		
		$contestsex = $request->contest_sex;
		
		$contestaddress = $request->contest_address;
		
		$dishname = $request->contest_recpname;

		$dishingrdients = $request->contest_ingredients;
		
		$dishpreparation  = $request->contest_methprep;
		
		$tempid  = 5;

		$dishimage = $request->contest_recupimage;
		
		if(isset($dishimage)) {
		
			request()->validate([
	
				'contest_recupimage' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
	
			]);
	
			$imageName = time().'.'.request()->contest_recupimage->getClientOriginalExtension();
	
			request()->contest_recupimage->move(public_path('public/images/dishes'), $imageName);
		
		} else {
			
			$imageName = '';
				
		}
		
		$dishvideo = $request->contest_upvideo;
		
		if(isset($dishvideo)) {

			$videoName = time().'.'.request()->contest_upvideo->getClientOriginalExtension();
	
			request()->contest_upvideo->move(public_path('public/images/dishes'), $videoName);
		
		}
		
		else {
		
			$videoName = '';
		
		}
		
		$dishdoc = $request->contest_updoc;
		
		if(isset($dishdoc)) {

			$docName = time().'.'.request()->contest_updoc->getClientOriginalExtension();
	
			request()->contest_updoc->move(public_path('public/images/dishes'), $docName);
		
		}
		
		else {
		
			$docName = '';
		
		}
		
		//$this->postImage->add($input);
		
		//echo $imageName;
		//exit;

		

		$pid = DB::table('cookbook')->insertGetId(

			['cbname' => $contestname, 'cbemail' => $contestemail, 'cbphone' => $contestphone, 'cbaddress' => $contestaddress, 'cbsex' => $contestsex, 'cbrecipename' => $dishname, 'cbrecipeimage' => $imageName, 'cbrecipeincrediant' => $dishingrdients, 'cbmethodofpreperation' => $dishpreparation, 'cbrecipevideo' => $videoName, 'cbstatuscheck' => 1, 'cbdate' => date('Y-m-d')]

		);
		
		
		$string = str_replace(' ', '-', $dishname); // Replaces all spaces with hyphens.

  		$cmsslug = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
		
		$slugval = $this->create_slug($cmsslug,$cmsslug);
			
		DB::table('page_cms')
            ->insert(
			['pageid' => $pid, 'cmstemplate' => $tempid, 'cmsslug' => $slugval, 'cmsdet' => 2, 'tableid' => 1]
			);
			
        $content = [

		"contestname" => $contestname,
		
		"contestphone" => $contestphone,
		
		"contestemail" => $contestemail,

		"dishname" => $dishname,

		'logo'  => asset('public/frontend/images/sooryalogo.png')

		];

         Mail::to($ad_email)->send(new ContestMail($content));			

// 		//return 1;
// 		Session::flash('message','Thanks for participating!');
//               return Redirect::back();
         $redirectSlug = $this->getSlug('43');

         return Redirect::to($redirectSlug);


    }
	
	public function sent_contact(Request $request){

		 $email = "soorya@7harvestme.com";
		
		$content = [

		'contactname' => $request->contact_name,
		
		'contactphone' => $request->contact_phone,
		
		'contactemail' => $request->contact_email,
		
		'contactmessage' => $request->contact_message,

		'logo'  => asset('public/frontend/images/sooryalogo.png')

		];	
            
         Mail::to($email)->send(new ContactMail($content));

         $redirectSlug = $this->getSlug('35');

         return Redirect::to($redirectSlug);
    }
	
	
	public function poll_save(Request $request){

		$ipaddress = $_SERVER['REMOTE_ADDR'];
		
		$pollcheck = $request->pollcheck;
		
		$pollid = $request->hidpollid;		
		
		$pollcount = DB::table('poll')
							->leftjoin('poll_update','pollid','=','poll.plid')	
							->where('poll.plip','=',$ipaddress)
							->get();
		$plid = 0;
		$ip = 0;
		$cke = 0;	

		if(count($pollcount) > 0){
			$plid = $pollcount[0]->plid;
			$ip = $pollcount[0]->plip;
			$cke = $pollcount[0]->poll_cookie;
			if($ipaddress==$ip && (isset($_COOKIE['SooryaVoting']))){
			
			
			Session::flash('message','Sorry Already Voted!');
			
			}
			elseif($ipaddress==$ip && (!isset($_COOKIE['SooryaVoting']))){

			$cookie_name = "SooryaVoting";
			$cookie_value = "Soorya-".$ipaddress;

			setcookie($cookie_name, $cookie_value, time() + (604800 * 30), "/");

						$cookieID = DB::table('poll_update')
								->insert([
									"pollid" => $plid,
									"poll_cookie" => $cookie_value
								]);

			Session::flash('message','Thanks for Voting!');
			
		}
		}
		else{

			$cookie_name = "SooryaVoting";
			$cookie_value = "Soorya-".$ipaddress;

			setcookie($cookie_name, $cookie_value, time() + (604800 * 30), "/"); // 86400 = 1 day 
			
			// dd($cookie_name);

			$voteid = DB::table('poll')
							->insertGetId(
							['plrating' => $pollcheck, 'pollidlink' => $pollid, 'plip' => $ipaddress]
							);

			$cookieID = DB::table('poll_update')
								->insert([
									"pollid" => $voteid,
									"poll_cookie" => $cookie_value
									]);

	
			//return 1;
			Session::flash('message','Thanks for Voting!');
				
		}
        return Redirect::back();
	
    }
	
	
	public function news_save(Request $request){

		$newsemail = $request->newsemail;
		
		$newscount = DB::table('newsletter')->where('nwsemail','=',$newsemail)->count();
	
		
		if($newscount!=0) {
	
			// echo 0;
			Session::flash('message','Sorry Already Subscribed!');
			
		} else {

			//mail to user


			$content = [

        		'logo'  => asset('public/frontend/images/sooryalogo.png')

				];

		    	Mail::to($newsemail)->send(new NewsletterMail($content));

			
			//mail to admin
		       
			 $email = "soorya@7harvestme.com";
		      //  $email = "vipul@netstager.in";
		        $content = [
				
				'newsemail' => $newsemail,
		
				'logo'  => asset('public/frontend/images/sooryalogo.png')
		
				];
		
				Mail::to($email)->send(new NewsletterAdminMail($content));



			DB::table('newsletter')
				->insert(
				['nwsemail' => $newsemail]
				);	
			
			// echo 1;
			Session::flash('message','Thanks for subscribing!');


				
		}
              return Redirect::back();


    }
	
	
	public function create_slug($slug_url, $org_slug, $update=NULL){

      if($update){
        
          $count = DB::table('page_cms')->where('cmsslug', $slug_url)->count();

          if ($count > 1) {
            $tmp = explode("-",$slug_url);
            $checknum = end($tmp);
          
            if(!is_numeric($checknum)) {
              $num = 1; 
            }
            else {
               $num = $checknum +1;  
            }

            $slug_url = $org_slug."-".$num;

            return  $this->create_slug($slug_url,$org_slug,$update=NULL);
          } 
          else {
            return $slug_url;
          }

      

      }
      else{

        $count = DB::table('page_cms')->where('cmsslug', $slug_url)->count();
        if ($count > 0) {
          $tmp = explode("-",$slug_url);
          $checknum = end($tmp);
      
          if(!is_numeric($checknum)) {
            $num = 1; 
          }
          else{
            $num = $checknum +1;  
          }
          $slug_url = $org_slug."-".$num;
          return  $this->create_slug($slug_url,$org_slug,$update=NULL);
        } 
        else {
            return $slug_url;
        } 
      }
      
    }

	  public function getSlug($slugId){

      $arrSlugUserRegSlug  = DB::table('page_cms')
                                   ->where('cmsid',$slugId)
                                   ->select('cmsid','cmsslug')
                                   ->get();

      $userRegSlug = $arrSlugUserRegSlug[0]->cmsslug;

      return  $userRegSlug;
  }
	 

}

