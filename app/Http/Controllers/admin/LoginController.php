<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoginController
{
    public function login(){
        return view('admin.login');
    }

    public function doLogin(){
        $input = request()->except('_token');
    //    return $input;
        if(auth()->attempt($input)){
         
            return redirect()->route('dashboard');
        }else{   
            // dd(1);
            return redirect()->route('login')->with('message','Login Failed, Please check your credentials');
        }
    }
    public function dashboard(){
      
        return view('admin.home');
    }

    public function logout(){
        auth()->logout();
        return redirect()->route('login')->with('message','Succesfully Loggedout');
    }
}
