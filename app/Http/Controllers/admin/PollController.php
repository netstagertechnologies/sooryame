<?php



namespace App\Http\Controllers\admin;



use Illuminate\Http\Request;

use App\Http\Controllers\Controller;



use Illuminate\Support\Facades\DB;

use View;

use Crypt;

use Redirect;



class PollController extends Controller

{

    /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function __construct()

    {

        $this->middleware('auth');

    }



    /**

     * Show the application dashboard.

     *

     * @return \Illuminate\Http\Response

     */

    public function index()

    {

        return view('admin.pollcontest');

    }

	
	public function view_poll()

    {
		
		$strActiveMenu = "listPgs";
		
		$sliders = DB::table('pollsection')->select('*')->get();
		
		return view::make('admin.pollview')
    					->with('data',$sliders)
                        ->with('strActiveMenu',$strActiveMenu);
		
		

    }
	

	public function add_poll()

    {

		$strActiveMenu = "listPgs";

		//return 1;
		$temcms = DB::table('template_cms')->select('*')->get();
		
		return view::make('admin.addpoll')
    					->with('data',$temcms)
                        ->with('strActiveMenu',$strActiveMenu);
						
        //return view('admin.addpage');

    }

	

	public function save_poll(Request $request)

    {

		$pagetitle = $request->page_title;

		$pagecnt = $request->page_cnt;
		
		$tempid  = $request->template_sel;

		//$banimage = $request->banner_image;
		
		request()->validate([

            'banner_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);

        $imageName = time().'.'.request()->banner_image->getClientOriginalExtension();

        request()->banner_image->move(public_path('public/images/slider'), $imageName);

		//$this->postImage->add($input);
		
		//echo $imageName;
		//exit;

		

		$pid = DB::table('pollsection')->insertGetId(

			['polltitle' => $pagetitle, 'pollcontent' => $pagecnt, 'pollbanner' => $imageName]

		);
		
		
		$string = str_replace(' ', '-', $pagetitle); // Replaces all spaces with hyphens.

   		$cmsslug = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
		
		$slugval = $this->create_slug($cmsslug,$cmsslug);
			
		DB::table('page_cms')
            ->insert(
			['pageid' => $pid, 'cmstemplate' => $tempid, 'cmsslug' => $slugval, 'cmsdet' =>3]
			);	

		//return 1;

       return redirect()->route('list_poll');


    }
	
	public function editsave_poll(Request $request)

    {

		$pagetitle = $request->page_title;

		$pagecnt = $request->page_cnt;

		$pageid = $request->pageid;
		
		$banimg  = $request->banner_image;
		
		//$tempid  = $request->template_sel;
		
		//echo $banimg;
		//exit;
		
		//echo $banimg;
		//exit;
		
		if($banimg!='') {
		
			request()->validate([

				'banner_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

			]);

			$imageName = time().'.'.request()->banner_image->getClientOriginalExtension();

			request()->banner_image->move(public_path('public/images/slider'), $imageName);

			//$this->postImage->add($input);
			
			//echo $imageName;
			//exit;
		} else {
				$imageName = $request->bannerimage;
		}

		
		//echo $pageid;
		//exit;
		
		DB::table('pollsection')
            ->where('pollid', $pageid)
            ->update(
			['polltitle' => $pagetitle, 'pollcontent' => $pagecnt, 'pollbanner' => $imageName]
			);
		
		/*$string = str_replace(' ', '-', $pagetitle); // Replaces all spaces with hyphens.

   		$cmsslug = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
		
		$slugval = $this->create_slug($cmsslug,$cmsslug,1);
			
		DB::table('page_cms')
            ->where('pageid', $pageid)
			->where('cmsdet', 3)
            ->update(
			['cmstemplate' => $tempid, 'cmsslug' => $slugval]
			);	*/

		//return 1;

       return redirect()->route('list_poll');


    }

	

	public function edit_poll($id){
		
		$id = Crypt::decrypt($id);
		
		$strActiveMenu = "listPgs";
		
		//$sliders = DB::table('pages')->select('*')->where('pid', '=', $id)->get();
		
		/*$sliders = DB::table('pollsection')
            ->join('page_cms', 'pages.pid', '=', 'page_cms.pageid')
            ->select('pages.*', 'page_cms.*')
			->where('page_cms.cmsdet', '=', 1)
			->where('pages.pid', '=', $id)
            ->get();*/
		
		$sliders = DB::table('pollsection')
            ->select('pollsection.*')
			->where('pollsection.pollid', '=', $id)
            ->get();	
		
		$temcms = DB::table('template_cms')->select('*')->get();
		
		return view::make('admin.polledit')
    					->with('data',$sliders)
						->with('tempdata',$temcms)
                        ->with('strActiveMenu',$strActiveMenu);

		//echo $id;

	

	}
	
	public function del_page($id){
		
		$id = Crypt::decrypt($id);
		
		DB::table('pollsection')->where('pollid', '=', $id)->delete();
		
		DB::table('page_cms')->where('pageid', '=', $id)->where('cmsdet', '=', 1)->delete();

		//echo $id;

		return redirect()->route('list_poll');

	}

	public function create_slug($slug_url, $org_slug, $update=NULL){

      if($update){
        
          $count = DB::table('page_cms')->where('cmsslug', $slug_url)->count();

          if ($count > 1) {
            $tmp = explode("-",$slug_url);
            $checknum = end($tmp);
          
            if(!is_numeric($checknum)) {
              $num = 1; 
            }
            else {
               $num = $checknum +1;  
            }

            $slug_url = $org_slug."-".$num;

            return  $this->create_slug($slug_url,$org_slug,$update=NULL);
          } 
          else {
            return $slug_url;
          }

      

      }
      else{

        $count = DB::table('page_cms')->where('cmsslug', $slug_url)->count();
        if ($count > 0) {
          $tmp = explode("-",$slug_url);
          $checknum = end($tmp);
      
          if(!is_numeric($checknum)) {
            $num = 1; 
          }
          else{
            $num = $checknum +1;  
          }
          $slug_url = $org_slug."-".$num;
          return  $this->create_slug($slug_url,$org_slug,$update=NULL);
        } 
        else {
            return $slug_url;
        } 
      }
      
    }
    
        public function manage_poll($id){

    	$strActiveMenu = "listPgs";

    	$id = Crypt::decrypt($id); 

    	$pollData = DB::table('pollsection as PS')
    						->leftjoin('poll as PL','PL.pollidlink','=','PS.pollid')
    						->groupBy('PL.plrating')
    						->select('*',DB::raw('GROUP_CONCAT(PL.plrating) as votes'),DB::raw('COUNT(PL.plrating) as votecnt'))
    						->where('PS.pollid',$id)->get();

    				// 	dd($pollData);
    	$voteCount = DB::table('votes')
    					->where('votepollid',$id)
    					->get();
// dd($voteCount);
$badvote = 0;
$okvote = 0;
$goodvote = 0;
$exVote	= 0;


if(sizeof($voteCount) != ''){

// $badvote = $voteCount[0]->bad;
$badvote = 0;
$okvote = $voteCount[0]->ok;
$goodvote = $voteCount[0]->good;
$exVote = $voteCount[0]->excellent;
} 

    // 	$badCount = $pollData[0]->votecnt;
	   // $badTot = $badCount+$badvote;
	   $badCount =0;
        $badTot = 0;
    	$okCount = $pollData[1]->votecnt;
    	$okTot = $okCount+$okvote;

    	$goodCount = $pollData[2]->votecnt;
    	$goodTot = $goodCount+$goodvote;

    	$exCount = $pollData[3]->votecnt;
    	$exTot = $exCount+$exVote;
    	
 

		return view::make('admin.managepollview')
						->with('pollid',$id)
    					->with('badCount',$badCount)
						->with('okCount',$okCount)
						->with('goodCount',$goodCount)
						->with('exCount',$exCount)
						->with('badTot',$badTot)
						->with('okTot',$okTot)
						->with('goodTot',$goodTot)
						->with('exTot',$exTot)
                        ->with('strActiveMenu',$strActiveMenu);

}

	public function save_votes(Request $request){

		// dd($request->all());
		$pollid = $request->pollid;

		$requestData = [
							"bad" => $request->bad_count,
							"ok"  => $request->ok_count,
							"good" => $request->good_count,
							"excellent" => $request->ex_count
						];

		$Vdata = DB::table('votes')
					->where('votepollid',$pollid)
					->get();


		if ($Vdata->count() > 0) {

		$upData =	DB::table('votes')
					->where('votepollid',$pollid)
					->update($requestData);


		if($upData)
		        $msg = 'Edit-SUCCESS'; 
		      else
		        $msg = ''; 
		}else{

		$inData = DB::table('votes')->insert(["votepollid" => $request->pollid,
										"bad" => $request->bad_count,
										"ok"  => $request->ok_count,
										"good" => $request->good_count,
										"excellent" => $request->ex_count
										]);

		if($inData)
		        $msg = 'Add-SUCCESS'; 
		else
		        $msg = ''; 
		}

		// return redirect()->route('list_poll');
		return Redirect::to(route('list_poll', array('message' => $msg)));

	}

}

