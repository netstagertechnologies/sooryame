<?php



namespace App\Http\Controllers\admin;



use Illuminate\Http\Request;

use App\Http\Controllers\Controller;



use Illuminate\Support\Facades\DB;

use View;

use Crypt;



class DiscController extends Controller

{

    /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function __construct()

    {

        $this->middleware('auth');

    }



    /**

     * Show the application dashboard.

     *

     * @return \Illuminate\Http\Response

     */

    public function index()

    {

        return view('admin.disc');

    }

	
	public function view_disc()

    {
		
		$strActiveMenu = "listPgs";
		
		$sliders = DB::table('discoveries')->select('*')->get();
		
		return view::make('admin.discview')
    					->with('data',$sliders)
                        ->with('strActiveMenu',$strActiveMenu);
		
		

    }
	

	public function add_disc(){

		$strActiveMenu = "listPgs";
		$temcms = DB::table('template_cms')->select('*')->get();
		
		return view::make('admin.adddisc')
    					->with('data',$temcms)
                        ->with('strActiveMenu',$strActiveMenu);

    }

	

	public function save_disc(Request $request){

		$disctitle = $request->disc_title;
		$disccnt = $request->disc_cnt;		
		$tempid  = $request->template_sel;		
		$discimage = $request->disc_image;
		$metatitle = $request->meta_title;
		$metakeyword = $request->meta_keyword;
		$metadesc = $request->meta_desc;
		if(isset($discimage)) {
		
			request()->validate([
	
				'disc_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
	
			]);
	
			$imageName = time().'.'.request()->disc_image->getClientOriginalExtension();
	
			request()->disc_image->move(public_path('images/dishes'), $imageName);
		
		} else {
			
			$imageName = '';
				
		}
		
		$discvideo = $request->disc_video;
		
		if(isset($discvideo)) {

			$videoName = time().'.'.request()->disc_video->getClientOriginalExtension();
	
			request()->disc_video->move(public_path('images/dishes'), $videoName);
		
		}
		
		else {
		
			$videoName = '';
		
		}
		

		//$this->postImage->add($input);
		
		//echo $imageName;
		//exit;

		

		$did = DB::table('discoveries')->insertGetId(

			['dtitle' => $disctitle, 'dcontent' => $disccnt, 'dbanner' => $imageName, 'dvideo' => $videoName,'dmetatitle'=>$metatitle,'dmetakeyword'=>$metakeyword,'dmetadesc'=>$metadesc]

		);
		
		
		$string = str_replace(' ', '-', $disctitle); // Replaces all spaces with hyphens.

   		$cmsslug = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
		
		$slugval = $this->create_slug($cmsslug,$cmsslug);
			
		DB::table('page_cms')
            ->insert(
			['pageid' => $did, 'cmstemplate' => $tempid, 'cmsslug' => $slugval, 'cmsdet' => 4, 'tableid' => 2]
			);	

		//return 1;

       return redirect()->route('list_disc');


    }
	
	public function editsave_disc(Request $request){
		$disctitle = $request->disc_title;

		$disccnt = $request->disc_cnt;

		$pageid = $request->pageid;
		
		$tempid  = $request->template_sel;
		
		$discimageold = $request->discimage;
		
		$discvideoold = $request->discvideo;
		
		$discimage = $request->disc_image;
		$metatitle = $request->meta_title;
		$metakeyword = $request->meta_keyword;
		$metadesc = $request->meta_desc;
		if(isset($discimage)) {
		
			request()->validate([

				'disc_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

			]);

			$imageName = time().'.'.request()->disc_image->getClientOriginalExtension();

			request()->disc_image->move(public_path('images/dishes'), $imageName);
		} else {
			
			if(isset($discimageold)) {
				$imageName = $discimageold;
			} else {
				$imageName = '';
			}
			
		}
		
		$discvideo = $request->disc_video;
		
		
		if(isset($discvideo)) {
			
	
				$videoName = time().'.'.request()->disc_video->getClientOriginalExtension();
		
				request()->disc_video->move(public_path('images/dishes'), $videoName);
			
			
			
		} else {
				
				if(isset($discvideoold)) {
					$videoName = $discvideoold;
				} else {
					$videoName = '';
				}
			
		}

		
		
		DB::table('discoveries')
            ->where('did', $pageid)
            ->update(
			['dtitle' => $disctitle, 'dcontent' => $disccnt, 'dbanner' => $imageName, 'dvideo' => $videoName,'dmetatitle'=>$metatitle,'dmetakeyword'=>$metakeyword,'dmetadesc'=>$metadesc]
			);
		
		$string = str_replace(' ', '-', $disctitle); // Replaces all spaces with hyphens.

   		$cmsslug = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
		
		$slugval = $this->create_slug($cmsslug,$cmsslug,1);
			
		DB::table('page_cms')
            ->where('pageid', $pageid)
			->where('cmsdet', 4)
            ->update(
			['cmstemplate' => $tempid, 'cmsslug' => $slugval]
			);	

		//return 1;

       return redirect()->route('list_disc');


    }

	

	public function edit_disc($id){
		
		$id = Crypt::decrypt($id);

		
		$strActiveMenu = "listPgs";
		
		
		$sliders = DB::table('discoveries')
            ->join('page_cms', 'discoveries.did', '=', 'page_cms.pageid')
            ->select('discoveries.*', 'page_cms.*')
			->where('page_cms.cmsdet', '=', 4)
			->where('discoveries.did', '=', $id)
            ->get();
			
		$temcms = DB::table('template_cms')->select('*')->get();
		
		return view::make('admin.discedit')
    					->with('data',$sliders)
						->with('tempdata',$temcms)
                        ->with('strActiveMenu',$strActiveMenu);

		//echo $id;

	

	}
	
	public function del_disc($id){
		
		$id = Crypt::decrypt($id);
		
		DB::table('discoveries')->where('did', '=', $id)->delete();
		
		DB::table('page_cms')->where('pageid', '=', $id)->where('cmsdet', '=', 4)->delete();

		//echo $id;

		return redirect()->route('list_disc');

	}

	public function create_slug($slug_url, $org_slug, $update=NULL){

      if($update){
        
          $count = DB::table('page_cms')->where('cmsslug', $slug_url)->count();
		  
		  //echo $count;

          if ($count > 1) {
            $tmp = explode("-",$slug_url);
            $checknum = end($tmp);
          
            if(!is_numeric($checknum)) {
              $num = 1; 
            }
            else {
               $num = $checknum +1;  
            }

            $slug_url = $org_slug."-".$num;

            return  $this->create_slug($slug_url,$org_slug,$update=NULL);
          } 
          else {
            return $slug_url;
          }

      

      }
      else{

        $count = DB::table('page_cms')->where('cmsslug', $slug_url)->count();
        if ($count > 0) {
          $tmp = explode("-",$slug_url);
          $checknum = end($tmp);
      
          if(!is_numeric($checknum)) {
            $num = 1; 
          }
          else{
            $num = $checknum +1;  
          }
          $slug_url = $org_slug."-".$num;
          return  $this->create_slug($slug_url,$org_slug,$update=NULL);
        } 
        else {
            return $slug_url;
        } 
      }
      
    }

}

