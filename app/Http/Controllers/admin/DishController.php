<?php



namespace App\Http\Controllers\admin;



use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use View;

use Crypt;

class DishController extends Controller

{

    /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function __construct()

    {

        $this->middleware('auth');

    }



    /**

     * Show the application dashboard.

     *

     * @return \Illuminate\Http\Response

     */

    public function index()

    {

        return view('admin.page');

    }

	
	public function view_dishes()

    {
		
		$strActiveMenu = "listPgs";
		
		$sliders = DB::table('cookbook')->select('*')->where('cbstatuscheck',0)->get();
		
		return view::make('admin.dishview')
    					->with('data',$sliders)
                        ->with('strActiveMenu',$strActiveMenu);
		
		

    }
	

	public function add_dish()

    {

		$strActiveMenu = "listPgs";

		//return 1;
		$temcms = DB::table('template_cms')->select('*')->get();
		
		return view::make('admin.adddishes')
    					->with('data',$temcms)
                        ->with('strActiveMenu',$strActiveMenu);
						
        //return view('admin.addpage');

    }

	

	public function save_dish(Request $request){
		
		$dishname = $request->dish_name;

		$dishingrdients = $request->dish_ingrdients;
		
		$dishpreparation  = $request->dish_preparation;
		
		$dishcontent  = $request->dish_content;
		
		$tempid  = $request->template_sel;

		$dishimage = $request->dish_image;
		$metatitle = $request->meta_title;
		$metakeyword = $request->meta_keyword;
		$metadesc = $request->meta_desc;
		
		if(isset($dishimage)) {
		
			request()->validate([
	
				'dish_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
	
			]);
	
			$imageName = time().'.'.request()->dish_image->getClientOriginalExtension();
	
			request()->dish_image->move(public_path('images/dishes'), $imageName);
		
		} else {
			
			$imageName = '';
				
		}
		
		$dishvideo = $request->dish_video;
		
		if(isset($dishvideo)) {

			$videoName = time().'.'.request()->dish_video->getClientOriginalExtension();
	
			request()->dish_video->move(public_path('images/dishes'), $videoName);
		
		}
		
		else {
		
			$videoName = '';
		
		}
		
	
		

		$pid = DB::table('cookbook')->insertGetId(

			['cbrecipename' => $dishname, 'cbrecipeimage' => $imageName, 'cbrecipeincrediant' => $dishingrdients,
			 'cbmethodofpreperation' => $dishpreparation, 'cbrecipecontent' => $dishcontent,
			  'cbrecipevideo' => $videoName, 'cbstatuscheck' => 0,
			   'cbdate' => date('Y-m-d'),'meta_title'=>$metatitle,'meta_keyword'=>$metakeyword,
			   'meta_description'=>$metadesc]

		);
		
		
		$string = str_replace(' ', '-', $dishname); // Replaces all spaces with hyphens.

   		$cmsslug = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
		
		$slugval = $this->create_slug($cmsslug,$cmsslug);
			
		DB::table('page_cms')
            ->insert(
			['pageid' => $pid, 'cmstemplate' => $tempid, 'cmsslug' => $slugval, 'cmsdet' => 2, 'tableid' => 1]
			);	

		//return 1;

       return redirect()->route('list_dishes');


    }
	
	public function editsave_dish(Request $request){
		$dishname = $request->dish_name;
		$dishingrdients = $request->dish_ingrdients;		
		$dishpreparation  = $request->dish_preparation;		
		$cbrecipecontent  = $request->dish_content;		
		$tempid  = $request->template_sel;
		$dishimageold = $request->dishimage;		
		$dishvideoold = $request->dishvideo;		
		$dishid = $request->dishid;
		$metatitle = $request->meta_title;
		$metakeyword = $request->meta_keyword;
		$metadesc = $request->meta_desc;		
		$dishimage = $request->dish_image;		
		if(isset($dishimage)) {
		
			request()->validate([

				'dish_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

			]);

			$imageName = time().'.'.request()->dish_image->getClientOriginalExtension();

			request()->dish_image->move(public_path('images/dishes'), $imageName);
		} else {
			
			if(isset($dishimageold)) {
				$imageName = $dishimageold;
			} else {
				$imageName = '';
			}
			
		}
		
		$dishvideo = $request->dish_video;
		
		
		if(isset($dishvideo)) {			
	
				$videoName = time().'.'.request()->dish_video->getClientOriginalExtension();
		
				request()->dish_video->move(public_path('images/dishes'), $videoName);			
			
			
		} else {
				
				if(isset($dishvideoold)) {
					$videoName = $dishvideoold;
				} else {
					$videoName = '';
				}
			
		}
		
		DB::table('cookbook')
            ->where('cbid', $dishid)
            ->update(
			['cbrecipename' => $dishname, 'cbrecipeimage' => $imageName, 'cbrecipeincrediant' => $dishingrdients,
			 'cbmethodofpreperation' => $dishpreparation, 'cbrecipecontent' => $cbrecipecontent, 'cbrecipevideo' => $videoName,'meta_title'=>$metatitle,'meta_keyword'=>$metakeyword,'meta_description'=>$metadesc]
			);
		
		$string = str_replace(' ', '-', $dishname); // Replaces all spaces with hyphens.

   		$cmsslug = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
		
		$slugval = $this->create_slug($cmsslug,$cmsslug,1);
			
		DB::table('page_cms')
            ->where('pageid', $dishid)
			->where('cmsdet', 2)
            ->update(
			['cmstemplate' => $tempid, 'cmsslug' => $slugval]
			);	
       return redirect()->route('list_dishes');


    }
	public function edit_dish($id){
		
		$id = Crypt::decrypt($id);		
		$strActiveMenu = "listPgs";		
		$sliders = DB::table('cookbook')
            ->join('page_cms', 'cookbook.cbid', '=', 'page_cms.pageid')
            ->select('cookbook.*', 'page_cms.*')
			->where('page_cms.cmsdet', '=', 2)
			->where('cookbook.cbid', '=', $id)
            ->get();
		$temcms = DB::table('template_cms')->select('*')->get();
		
		return view::make('admin.dishedit')
    					->with('data',$sliders)
						->with('tempdata',$temcms)
						->with('strActiveMenu',$strActiveMenu);
					

	

	}
	
	public function del_dish($id){
		
		$id = Crypt::decrypt($id);
		
		DB::table('cookbook')->where('cbid', '=', $id)->delete();
		
		DB::table('page_cms')->where('pageid', '=', $id)->where('cmsdet', '=', 2)->delete();

		return redirect()->route('list_dishes');

	}

	public function create_slug($slug_url, $org_slug, $update=NULL){

      if($update){
        
          $count = DB::table('page_cms')->where('cmsslug', $slug_url)->count();

          if ($count > 1) {
            $tmp = explode("-",$slug_url);
            $checknum = end($tmp);
          
            if(!is_numeric($checknum)) {
              $num = 1; 
            }
            else {
               $num = $checknum +1;  
            }

            $slug_url = $org_slug."-".$num;

            return  $this->create_slug($slug_url,$org_slug,$update=NULL);
          } 
          else {
            return $slug_url;
          }

      

      }
      else{

        $count = DB::table('page_cms')->where('cmsslug', $slug_url)->count();
        if ($count > 0) {
          $tmp = explode("-",$slug_url);
          $checknum = end($tmp);
      
          if(!is_numeric($checknum)) {
            $num = 1; 
          }
          else{
            $num = $checknum +1;  
          }
          $slug_url = $org_slug."-".$num;
          return  $this->create_slug($slug_url,$org_slug,$update=NULL);
        } 
        else {
            return $slug_url;
        } 
      }
      
    }

}

