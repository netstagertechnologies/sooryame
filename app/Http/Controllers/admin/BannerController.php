<?php



namespace App\Http\Controllers\admin;



use Illuminate\Http\Request;

use App\Http\Controllers\Controller;



use Illuminate\Support\Facades\DB;

use View;

use Crypt;



class BannerController extends Controller

{

    /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function __construct()

    {

        $this->middleware('auth');

    }



    /**

     * Show the application dashboard.

     *

     * @return \Illuminate\Http\Response

     */

    public function index(){
    return view('admin.banner');
    }	
	public function view_ban(){
		
		$strActiveMenu = "listPgs";		
		$sliders = DB::table('slider')->select('*')->get();		
		return view::make('admin.bannerview')
    					->with('data',$sliders)
                        ->with('strActiveMenu',$strActiveMenu);	
        return view('admin.bannerview');

    }
	

	public function add_ban(){

        return view('admin.addbanner');

    }

	

	public function save_ban(Request $request){

		$banname = $request->banner_name;

		$bantitle = $request->banner_title;

		$banlink = $request->banner_link;

		//$banimage = $request->banner_image;
		
		request()->validate([
            'banner_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);

        $imageName = time().'.'.request()->banner_image->getClientOriginalExtension();

        request()->banner_image->move(public_path('images/slider'), $imageName);

		//$this->postImage->add($input);
		
		//echo $imageName;
		//exit;

		

		DB::table('slider')->insert(

			['sname' => $banname, 'stitle' => $bantitle, 'slink' => $banlink, 'simage' => $imageName]

		);

		//return 1;

       return redirect()->route('list_banner');


    }
	
	public function editsave_ban(Request $request)

    {

		$banname = $request->banner_name;

		$bantitle = $request->banner_title;

		$banlink = $request->banner_link;

		$banid = $request->sliderid;
		
		$banimg  = $request->banner_image;
		
		if($banimg!='') {
		
			request()->validate([

				'banner_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

			]);

			$imageName = time().'.'.request()->banner_image->getClientOriginalExtension();

			request()->banner_image->move(public_path('images/slider'), $imageName);

			//$this->postImage->add($input);
			
			//echo $imageName;
			//exit;
		} else {
				$imageName = $request->slideriimage;
		}

		
		
		DB::table('slider')
            ->where('sid', $banid)
            ->update(
			['sname' => $banname, 'stitle' => $bantitle, 'slink' => $banlink, 'simage' => $imageName]
			);

		//return 1;

       return redirect()->route('list_banner');


    }

	

	public function edit_ban($id){
		
		$id = Crypt::decrypt($id);
		
		$strActiveMenu = "listPgs";
		
		$sliders = DB::table('slider')->select('*')->where('sid', '=', $id)->get();
		
		return view::make('admin.banneredit')
    					->with('data',$sliders)
                        ->with('strActiveMenu',$strActiveMenu);

		//echo $id;

	

	}
	
	public function del_ban($id){
		
		$id = Crypt::decrypt($id);
		
		DB::table('slider')->where('sid', '=', $id)->delete();

		//echo $id;

		return redirect()->route('list_banner');

	}

	

}

