<?php



namespace App\Http\Controllers\admin;



use Illuminate\Http\Request;

use App\Http\Controllers\Controller;



use Illuminate\Support\Facades\DB;

use View;

use Crypt;



class ThemeController extends Controller

{

    /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function __construct()

    {

        $this->middleware('auth');

    }



    /**

     * Show the application dashboard.

     *

     * @return \Illuminate\Http\Response

     */

    public function index(){

    return view('admin.themeoption');

    }
	public function view_theme(){
		
		$strActiveMenu = "listPgs";
		$data = DB::table('theme_options')->select('*')->get();
		$title = "Manage Theme Option";
		return view::make('admin.themeoption',compact('title','strActiveMenu','data'));
    }
	
	public function save_option(Request $request){

		$address = $request->address;
		$mobile = $request->mobile;
		$phone = $request->phone;		
		$email = $request->email;
		$facebook = $request->facebook;
		$twitter = $request->twitter;		
		$content = $request->content;
		$instagram = $request->instagram;
		$youtube = $request->youtube;		
		$optid = $request->optionid;

		$counters = DB::table('theme_options')->count();
		
		if($counters==0) {		
			DB::table('theme_options')->insert(	
				['taddress' => $address, 'tmobile' => $mobile, 'tphone' => $phone, 'temail' => $email,
				 'tfacebook' => $facebook, 'ttwitter' => $twitter, 'tinstagram' => $instagram,
				  'tyoutube' => $youtube, 'tcontent' => $content]	
			);
			
		} else {
		
			DB::table('theme_options')
            ->where('tid', $optid)
            ->update(
				['taddress' => $address, 'tmobile' => $mobile, 'tphone' => $phone, 'temail' => $email,
				 'tfacebook' => $facebook, 'ttwitter' => $twitter, 'tinstagram' => $instagram, 
				 'tyoutube' => $youtube, 'tcontent' => $content]
			);
			
		}

       return redirect()->route('theme_option');


    }
	
	

	

}

