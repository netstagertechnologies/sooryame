<?php



namespace App\Http\Controllers\admin;



use Illuminate\Http\Request;

use App\Http\Controllers\Controller;



use Illuminate\Support\Facades\DB;

use View;

use Crypt;
class PageController extends Controller

{

    /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function __construct()

    {

        $this->middleware('auth');

    }


    public function index()

    {

        return view('admin.page');

    }

	
	public function view_page()

    {
		
		$strActiveMenu = "listPgs";
		
		$sliders = DB::table('pages')->select('*')->get();
		
		return view::make('admin.pageview')
    					->with('data',$sliders)
                        ->with('strActiveMenu',$strActiveMenu);
		
		

    }
	

	public function add_page()

    {

		$strActiveMenu = "listPgs";

		//return 1;
		$temcms = DB::table('template_cms')->select('*')->get();
		
		return view::make('admin.addpage')
    					->with('data',$temcms)
                        ->with('strActiveMenu',$strActiveMenu);
						
        //return view('admin.addpage');

    }

	

	public function save_page(Request $request)

    {

		$pagetitle = $request->page_title;
		$pagecnt = $request->page_cnt;		
		$tempid  = $request->template_sel;
		$metatitle = $request->meta_title;
		$metakeyword = $request->meta_keyword;
		$metadesc = $request->meta_desc;

		//$banimage = $request->banner_image;
		
		request()->validate([

            'banner_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);

        $imageName = time().'.'.request()->banner_image->getClientOriginalExtension();

        request()->banner_image->move(public_path('images/slider'), $imageName);

		$pid = DB::table('pages')->insertGetId(

			['title' => $pagetitle, 'content' => $pagecnt, 'banner' => $imageName,'meta_title'=>$metatitle,'meta_keyword'=>$metakeyword,'meta_description'=>$metadesc]

		);
		$string = str_replace(' ', '-', $pagetitle); // Replaces all spaces with hyphens.

   		$cmsslug = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
		
		$slugval = $this->create_slug($cmsslug,$cmsslug);
			
		DB::table('page_cms')
            ->insert(
			['pageid' => $pid, 'cmstemplate' => $tempid, 'cmsslug' => $slugval, 'cmsdet' => 1]
			);	

       return redirect()->route('list_page');


    }
	
	public function editsave_page(Request $request)

    {

		$pagetitle = $request->page_title;

		$pagecnt = $request->page_cnt;

		$pageid = $request->pageid;
		
		$banimg  = $request->banner_image;
		
		$tempid  = $request->template_sel;
		$metatitle = $request->meta_title;
		$metakeyword = $request->meta_keyword;
		$metadesc = $request->meta_desc;
				
		if($banimg!='') {
		
			request()->validate([

				'banner_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

			]);

			$imageName = time().'.'.request()->banner_image->getClientOriginalExtension();

			request()->banner_image->move(public_path('images/slider'), $imageName);

			//$this->postImage->add($input);
			
			//echo $imageName;
			//exit;
		} else {
				$imageName = $request->bannerimage;
		}

		
		
		DB::table('pages')
            ->where('pid', $pageid)
            ->update(
			['title' => $pagetitle, 'content' => $pagecnt, 'banner' => $imageName,'meta_title'=>$metatitle,'meta_keyword'=>$metakeyword,'meta_description'=>$metadesc]
			);
		
		$string = str_replace(' ', '-', $pagetitle); // Replaces all spaces with hyphens.

   		$cmsslug = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
		
		$slugval = $this->create_slug($cmsslug,$cmsslug,1);
			
		DB::table('page_cms')
            ->where('pageid', $pageid)
			->where('cmsdet', 1)
            ->update(
			['cmstemplate' => $tempid, 'cmsslug' => $slugval]
			);	

		//return 1;

       return redirect()->route('list_page');


    }

	

	public function edit_page($id){
		
		$id = Crypt::decrypt($id);
		
		$strActiveMenu = "listPgs";
		
		//$sliders = DB::table('pages')->select('*')->where('pid', '=', $id)->get();
		
		$sliders = DB::table('pages')
            ->join('page_cms', 'pages.pid', '=', 'page_cms.pageid')
            ->select('pages.*', 'page_cms.*')
			->where('page_cms.cmsdet', '=', 1)
			->where('pages.pid', '=', $id)
            ->get();
		//dd(	$sliders);
		$temcms = DB::table('template_cms')->select('*')->get();
		
		return view::make('admin.pageedit')
    					->with('data',$sliders)
						->with('tempdata',$temcms)
                        ->with('strActiveMenu',$strActiveMenu);

		//echo $id;

	

	}
	
	public function del_page($id){
		
		$id = Crypt::decrypt($id);
		
		DB::table('pages')->where('pid', '=', $id)->delete();
		
		DB::table('page_cms')->where('pageid', '=', $id)->where('cmsdet', '=', 1)->delete();

		//echo $id;

		return redirect()->route('list_page');

	}

	public function create_slug($slug_url, $org_slug, $update=NULL){

      if($update){
        
          $count = DB::table('page_cms')->where('cmsslug', $slug_url)->count();

          if ($count > 1) {
            $tmp = explode("-",$slug_url);
            $checknum = end($tmp);
          
            if(!is_numeric($checknum)) {
              $num = 1; 
            }
            else {
               $num = $checknum +1;  
            }

            $slug_url = $org_slug."-".$num;

            return  $this->create_slug($slug_url,$org_slug,$update=NULL);
          } 
          else {
            return $slug_url;
          }

      

      }
      else{

        $count = DB::table('page_cms')->where('cmsslug', $slug_url)->count();
        if ($count > 0) {
          $tmp = explode("-",$slug_url);
          $checknum = end($tmp);
      
          if(!is_numeric($checknum)) {
            $num = 1; 
          }
          else{
            $num = $checknum +1;  
          }
          $slug_url = $org_slug."-".$num;
          return  $this->create_slug($slug_url,$org_slug,$update=NULL);
        } 
        else {
            return $slug_url;
        } 
      }
      
    }

}

