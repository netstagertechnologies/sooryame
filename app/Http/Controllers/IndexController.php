<?php

namespace App\Http\Controllers;
use Request;
use DB;
use Seo;
use View;
use Session;

class IndexController extends Controller
{

	public function home(){
		return view('frontend.index');
	}
    public function index(){
	
        $data['slug']    = str_replace('_', '-', urldecode(Request::segment(1)));  //would be slug_id
        if($data['slug'] == ""){
        $meta= DB::table('page_cms as pcms')
		->leftjoin('pages as pgs', 'pgs.pid', '=', 'pcms.pageid')
		->where('pgs.pid',13)
		->get();  
	
		$title 	 =	$meta[0]->title;
		$keyword =	$meta[0]->meta_keyword;
		$desc	 =  $meta[0]->meta_description;
		}
		
		else{	
        $meta= DB::table('page_cms as pcms')
		->leftjoin('pages as pgs', 'pgs.pid', '=', 'pcms.pageid')
		->where('pcms.cmsslug',$data['slug'])
		->get(); 
		// dd($meta);
		if($meta->count() == 0){
			return redirect()->route('pagenotfound');
		}
		$text = str_replace('-', ' ', $data['slug']);
		$title 	 =	$text;
		$keyword =	$meta[0]->meta_keyword;
		$desc	 =	$meta[0]->meta_description;
		
        }
	
		Seo::set('title',$title);
		Seo::set('description',$desc);
		Seo::set('keywords',$keyword);
		
        $data['theme'] = DB::table('theme_options')
                          ->select('*')
                          ->get();

		$urlSlugs  = DB::table('page_cms as pcms')
								   ->select('cmsslug','cmsid')
								   ->get();
		foreach($urlSlugs as $url){
			$data['urlSlugs'][$url->cmsid] = $url->cmsslug;
		}

        if($data['slug']=="" || $data['slug']==NULL){

				$data['arrSlug']  = DB::table('page_cms as pcms')
								   ->join('template_cms as tcms', 'tcms.tcmsid', '=', 'pcms.cmstemplate')
								   ->select('tcmsname','tableid')
								   ->get();

                $data['strActiveIcn'] = '';                
                $this->hometemplate($data);

        } else{
				$data['arrSlug']  = DB::table('page_cms as pcms')
								   ->join('template_cms as tcms', 'tcms.tcmsid', '=', 'pcms.cmstemplate')
								   ->select('tcmsname','tableid')
								   ->where('pcms.cmsslug',$data['slug'])
									->first();
									if(isset($data['arrSlug'])){
				$tableid = $data['arrSlug']->tableid;
									}else{
										$tableid ="";
									}
				$slugurl = $data['slug'];
				if($tableid==1) {
					$data['slugData'] = DB::table('page_cms as pcms')
										->join('cookbook as ckbk', 'ckbk.cbid', '=', 'pcms.pageid')
										->join('template_cms as tcms', 'tcms.tcmsid', '=', 'pcms.cmstemplate')
										->select('pcms.*','tcms.*','ckbk.*')
										->where('pcms.cmsslug',$slugurl)
										->get();
				} elseif($tableid==2) {
					$data['slugData'] = DB::table('page_cms as pcms')
										->join('discoveries as disc', 'disc.did', '=', 'pcms.pageid')
										->join('template_cms as tcms', 'tcms.tcmsid', '=', 'pcms.cmstemplate')
										->select('pcms.*','tcms.*','disc.*')
										->where('pcms.cmsslug',$slugurl)
										->get();
				} else {
					$data['slugData'] = DB::table('page_cms as pcms')
										->join('pages as pgs', 'pgs.pid', '=', 'pcms.pageid')
										->join('template_cms as tcms', 'tcms.tcmsid', '=', 'pcms.cmstemplate')
										->select('pcms.*','pgs.*','tcms.*')
										->where('pcms.cmsslug',$slugurl)
										->get();

				}
                    
				if(sizeof($data['slugData'])>0){
                $data['template'] = $data['slugData'][0]->tcmsname;
				}else{
					return redirect()->route('pagenotfound');
					echo "pageno";
				}

                if($data['template']){
                  $fnname = $data['template'];
                    $this->$fnname($data);
                }
                else{
                   $this->load->view('dashboard.php', $data);
                }

        }

	}





public function pagenotfound1($data=array()){
	$data['slug']    = str_replace('_', '-', urldecode(Request::segment(1)));  //would be slug_id

	$meta= DB::table('page_cms as pcms')
	->leftjoin('pages as pgs', 'pgs.pid', '=', 'pcms.pageid')
	->where('pgs.pid',20)
	->get(); 
	$title 	 =	$meta[0]->meta_title;
	$keyword =	$meta[0]->meta_keyword;
	$desc	 =	$meta[0]->meta_description;
	Seo::set('title',$title);
	Seo::set('description',$desc);
	Seo::set('keywords',$keyword);
	$data['theme'] = DB::table('theme_options')
					->select('*')
					->get();

	$urlSlugs  = DB::table('page_cms as pcms')
							->select('cmsslug','cmsid')
							->get();
	foreach($urlSlugs as $url){
		$data['urlSlugs'][$url->cmsid] = $url->cmsslug;
	}
      echo View::make('pagenotfound')->with('pagedet',$data)
                                ->render();
	}

	public function innertemplate($data=array()){
		$meta= DB::table('page_cms as pcms')
		->leftjoin('pages as pgs', 'pgs.pid', '=', 'pcms.pageid')
		->where('pgs.pid',6)
		->get();
		// dd($meta);
		$title 	 =	$meta[0]->title;
		$keyword =	$meta[0]->meta_keyword;
		$desc	 =	$meta[0]->meta_description;
		Seo::set('title',$title);
		Seo::set('description',$desc);
		Seo::set('keywords',$keyword);
      $pageData['arrSlug'] = $data['arrSlug'];
		// dd($pageData);
      echo View::make('innertemplate')->with('pagedet',$data)
                                ->render();

    }

	public function cooktemplate($data=array()){
      $pageData['arrSlug'] = $data['arrSlug'];

	  $cookdet = DB::table('page_cms as pcms')
                 	->leftjoin('cookbook as ckbk', 'ckbk.cbid', '=', 'pcms.pageid')
                    ->select('pcms.*','ckbk.*')
					->where('pcms.cmsdet',2)
					->where('ckbk.cbstatuscheck',0)
					->get();
					
					$cookdet1 = DB::table('page_cms as pcms')				
				   ->select('pcms.*')
				   ->where('pcms.cmsdet',2)
				   ->get();
				
				   $meta= DB::table('page_cms as pcms')
				   ->leftjoin('pages as pgs', 'pgs.pid', '=', 'pcms.pageid')
				   ->where('pgs.pid',7)
				   ->get(); 
				   $title 	 =	$meta[0]->meta_title;
			   
				   $keyword =	$meta[0]->meta_keyword;
				   $desc	 =	$meta[0]->meta_description;
			
					Seo::set('title',$title);
					Seo::set('description',$desc);
					Seo::set('keywords',$keyword);

      echo View::make('cooktemplate')->with('pagedet',$data)
	  							->with('cookdet',$cookdet)
                                ->render();

	}

	public function cooksingle($data=array()){
	
      $pageData['arrSlug'] = $data['arrSlug'];
	  $cookdet = DB::table('pages as pg')
					->where('pg.pid',7)
                    ->get();

					$cook1=array();
				
		$title 	 =	$data['slugData'][0]->meta_title;

		$keyword =	$data['slugData'][0]->meta_keyword;
		$desc	 =	$data['slugData'][0]->meta_description;

		Seo::set('title',$title);
		Seo::set('description',$desc);
		Seo::set('keywords',$keyword);         
	
      echo View::make('cooksingle')->with('pagedet',$data)
	  						->with('cookdet',$cookdet)
	  						->with('cook1',$cook1)
                              			        ->render();

    }

	public function disctemplate($data=array()){

	  $pageData['arrSlug'] = $data['arrSlug']; 
	  $discdet = DB::table('page_cms as pcms')
	 				 ->leftjoin('pages as pgs', 'pgs.pid', '=', 'pcms.pageid')
                 	->join('discoveries as disc', 'disc.did', '=', 'pcms.pageid')
                    ->select('pcms.*','disc.*','pgs.*')
					->where('pcms.cmsdet',4)
					->get();
					$meta= DB::table('page_cms as pcms')
					->leftjoin('pages as pgs', 'pgs.pid', '=', 'pcms.pageid')
					->where('pgs.pid',8)
					->get(); 
					$title 	 =	$meta[0]->meta_title;			   
					$keyword =	$meta[0]->meta_keyword;
					$desc	 =	$meta[0]->meta_description;

					Seo::set('title',$title);
					Seo::set('description',$desc);
					Seo::set('keywords',$keyword);

			// dd($discdet);
      echo View::make('disctemplate')->with('pagedet',$data)
	  							->with('discdet',$discdet)
                                ->render();

    }

	public function discsingle($data=array()){ 
		$meta= DB::table('page_cms as pcms')
		->leftjoin('pages as pgs', 'pgs.pid', '=', 'pcms.pageid')
		->where('pgs.pid',8)
		->get(); 
		$data['slug']    = str_replace('_', '-', urldecode(Request::segment(1))); 
		$text = str_replace('-', ' ', $data['slug']);
		// dd($text);
		$title 	 =	$text;			   
		$keyword =	$text;
		$desc	 =	$text;
		Seo::set('title',$title);
		Seo::set('description',$desc);
		Seo::set('keywords',$keyword);
      $pageData['arrSlug'] = $data['arrSlug'];
	 	$discdet = DB::table('page_cms as pcms')
                 	->join('pages as pg', 'pg.pid', '=', 'pcms.pageid')
                    ->select('pcms.*','pg.*')
					->where('pg.pid',8)
                    ->get();
      echo View::make('discsingle')->with('pagedet',$data)
	  							->with('discdet',$discdet)
                                ->render();

    }

	public function hometemplate($data=array()){	 
      $pageData['arrSlug'] = $data['arrSlug'];
		$cookdet = DB::table('page_cms as pcms')
                 	->join('cookbook as ckbk', 'ckbk.cbid', '=', 'pcms.pageid')
                    ->select('pcms.*','ckbk.*')
					->where('pcms.cmsdet',2)
					->where('ckbk.cbstatuscheck',0)
					->limit(2)
                    ->get();
		
		$discdet = DB::table('page_cms as pcms')
                 	->join('discoveries as disc', 'disc.did', '=', 'pcms.pageid')
                    ->select('pcms.*','disc.*')
					->where('pcms.cmsdet',4)
					->limit(2)
                    ->get();

		$pgdet = DB::table('page_cms as pcms')
                 	->join('pages as pg', 'pg.pid', '=', 'pcms.pageid')
                    ->select('pcms.*','pg.*')
					->where('pcms.cmsdet',1)
                    ->get();

		$polldet = DB::table('pollsection as plsec')
                    ->select('plsec.*')
                    ->get();
                    
      echo View::make('welcome')->with('pagedet',$data)
	  							->with('cookdet',$cookdet)
	  							->with('discdet',$discdet)
								->with('pgdet',$pgdet)
								->with('polldet',$polldet)
                                ->render();

    }

	public function contesttemplate($data=array()){
      $pageData['arrSlug'] = $data['arrSlug'];
	  $pgdet = DB::table('cookbook as ckbk')
                 	->join('contest as cnst', 'cnst.cnstcookbookid', '=', 'ckbk.cbid')
                    ->select('ckbk.*','cnst.*')
					->orderBy('cnst.cnstresult')
                    ->get();
					$meta= DB::table('page_cms as pcms')
					->leftjoin('pages as pgs', 'pgs.pid', '=', 'pcms.pageid')
					->where('pgs.pid',12)
					->get();
					$title 	 =	$meta[0]->meta_title;			   
					$keyword =	$meta[0]->meta_keyword;
					$desc	 =	$meta[0]->meta_description;
				
					Seo::set('title',$title);
								  Seo::set('description',$desc);
								  Seo::set('keywords',$keyword);
    echo View::make('contesttemplate')->with('pagedet',$data)
	  									->with('cnstdet',$pgdet)
                                		->render();

    }

	public function contacttemplate($data=array()){
		$meta= DB::table('page_cms as pcms')
		->leftjoin('pages as pgs', 'pgs.pid', '=', 'pcms.pageid')
		->where('pgs.pid',14)
		->get();
		
		$title 	 =	$meta[0]->meta_title;			   
		$keyword =	$meta[0]->meta_keyword;
		$desc	 =	$meta[0]->meta_description;
		Seo::set('title',$title);
					  Seo::set('description',$desc);
					  Seo::set('keywords',$keyword);
      $pageData['arrSlug'] = $data['arrSlug'];

      echo View::make('contacttemplate')->with('pagedet',$data)
                                ->render();

    }

	function GetMAC(){
		ob_start();
	//Get the ipconfig details using system commond
	system('ipconfig /all');
	// Capture the output into a variable
	$mycomsys=ob_get_contents();
	// Clean (erase) the output buffer
	ob_clean();
	$find_mac = "Physical"; //find the "Physical" & Find the position of Physical text
	$pmac = strpos($mycomsys, $find_mac);
	// Get Physical Address
	$macaddress=substr($mycomsys,($pmac+36),17);
	//Display Mac Address
	return $mycomsys;
	}

	public function polltemplate($data=array()){

      $pageData['arrSlug'] = $data['arrSlug'];
	  $meta= DB::table('page_cms as pcms')
	  ->leftjoin('pages as pgs', 'pgs.pid', '=', 'pcms.pageid')
	  ->where('pgs.pid',15)
	  ->get();
	  $title 	 ="Vote for our Batter on your experience";

	  $keyword =	"Vote for our Batter on your experience";
	  $desc	 =	"Tell us your experience regarding the batter we provided of idli/dosa/uttapam, 
	  Vote your feedback at the portal provided.";
	 				Seo::set('title',$title);
					Seo::set('description',$desc);
					Seo::set('keywords',$keyword);
	  $pgdet = DB::table('pollsection as plsec')
                    ->select('plsec.*')
                    ->get();

        $voteCount = DB::table('votes')
	    				->get();

	   //	$badvotecount = $voteCount[0]->bad;
	   $badvotecount = 0;
	   	$goodvotecount = $voteCount[0]->good;
	   	$okvotecount = $voteCount[0]->ok;
	   	$exvotecount = $voteCount[0]->excellent;
	   	// dd($exvotecount);

		$pgdetexcelent = DB::table('pollsection as plsec')
                 	->rightJoin('poll as pl', 'pl.plid', '=', 'plsec.pollid')
                    ->select('pl.plrating as excelent')
					->where('pl.plrating',4)
                    ->count();

        $pgdetexcelent = $pgdetexcelent+$exvotecount;

		$pgdetgood = DB::table('pollsection as plsec')
                 	->rightJoin('poll as pl', 'pl.plid', '=', 'plsec.pollid')
                    ->select('pl.plrating as good')
					->where('pl.plrating',3)
                    ->count();
		$pgdetgood = $pgdetgood+$goodvotecount;
		$pgdetok = DB::table('pollsection as plsec')
                 	->rightJoin('poll as pl', 'pl.plid', '=', 'plsec.pollid')
                    ->select('pl.plrating as ok')
					->where('pl.plrating',2)
                    ->count();
		$pgdetok = 	$pgdetok+$okvotecount;
		$pgdetbad = DB::table('pollsection as plsec')
                 	->rightJoin('poll as pl', 'pl.plid', '=', 'plsec.pollid')
                    ->select('pl.plrating as bad')
					->where('pl.plrating',1)
                    ->count();
        $pgdetbad = 0;
      echo View::make('polltemplate')->with('pagedet',$data)
	  							->with('polldet',$pgdet)
								->with('pgdetexcelent',$pgdetexcelent)
								->with('pgdetgood',$pgdetgood)
								->with('pgdetok',$pgdetok)
								->with('pgdetbad',$pgdetbad)
                                ->render();

    }

      public function commontemplate($data=array()){
		

            $pageData['arrSlug'] = $data['arrSlug'];

            $page_id =  $data['slugData'][0]->pageid;
            $pg_desc = DB::table('page_cms')
               				->leftjoin('pages','pid','=','page_cms.pageid')
                            ->where('pageid',$page_id)
                            ->get();
              echo View::make('commontemplate')
              		  ->with('pagedet',$data)
                      ->with('pageData',$pageData)
                      ->with('pg_desc',$pg_desc)
                      ->render();
        }



}



