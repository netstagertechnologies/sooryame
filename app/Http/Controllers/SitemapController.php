<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;
use Request;
use DB;
use Seo;
use View;
use Session;

class SitemapController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($data=array())
    {


        $data['slug']    = str_replace('_', '-', urldecode(Request::segment(1)));  //would be slug_id

        $data['theme'] = DB::table('theme_options')
                          ->select('*')
                          ->get();

		$urlSlugs  = DB::table('page_cms as pcms')
								   ->select('cmsslug','cmsid')
								   ->get();
		foreach($urlSlugs as $url){
			$data['urlSlugs'][$url->cmsid] = $url->cmsslug;
        }

        $meta= DB::table('page_cms as pcms')
                    ->leftjoin('pages as pgs', 'pgs.pid', '=', 'pcms.pageid')
                    ->where('pgs.pid',19)
                    ->where('status',1)
                    ->get(); 
        		// dd( $meta);
                //     $title 	 =	$meta[0]->meta_title;

				// 	$keyword =	$meta[0]->meta_keyword;
				// 	$desc	 =	$meta[0]->meta_description;

			

					Seo::set('title','Sitemap, Soorya');
					Seo::set('description','Check the latest interesting updates from Soorya here. Visit our sitemap page to know the complete updates concerning the same.');
					// Seo::set('keywords',$keyword);
                 
       $meta= DB::table('page_cms as pcms')
            ->leftjoin('pages as pgs', 'pgs.pid', '=', 'pcms.pageid')
           ->where('cmsslug','NOT LIKE','%Thank%')
           ->where('cmsslug','NOT LIKE','%sitemap%')
           ->where('cmsslug','NOT LIKE','%Who-we-are%')
           ->where('cmsslug','NOT LIKE','%Know-Us%')
           ->where('cmsslug','NOT LIKE','%About-Us%')
           ->where('cmsslug','NOT LIKE','Home-Page')
           ->where('status',1)
            ->orderby('tableid')
             ->get();
         $meta1= DB::table('page_cms as pcms')
             ->leftjoin('pages as pgs', 'pgs.pid', '=', 'pcms.pageid')
             ->where('tableid','1')
             ->where('pcms.status',1)   
              ->get();
              $metadish= DB::table('page_cms as pcms')
          
              ->where('pcms.cmsdet','2')

               ->get();
               $metadisc= DB::table('page_cms as pcms')
         
                ->where('pcms.cmsdet','4')
                ->where('status',1)
                ->get();

                echo View::make('sitemap')->with('pagedet',$data)
                ->with('pages',$meta)
                ->with('sub',$metadish)
                ->with('sub11',$metadisc)
                ->render();

    }


}



