@extends('layouts.app')

@section ('content')


@php
$data = $pagedet['slugData'];
//echo '<pre>';
//print_r($data);
//exit; 
@endphp

@foreach($data as $datas)

<section class="inner_page dicover" style="background-image:url({{asset('public/frontend/images/banner_bg.jpg')}});">
<div class="wid">

<h1 class="inercmnhed">{{$datas->title}}</h1>
<div class="inerbanimg"><img src="{{URL::asset('public/images/slider/'.$datas->banner)}}"></div>

</div>
</section>

@endforeach

<style>
.popup .popuptext {
visibility: hidden;
background-color: #555;
color: #fff;
text-align: center;
border-radius: 6px;
padding: 8px 0;
z-index: 1;
bottom: 125%;
left: 50%;
margin-left: -80px;
}
.popup .show {
visibility: visible;
-webkit-animation: fadeIn 1s;
animation: fadeIn 1s;
}
@-webkit-keyframes fadeIn {
from {opacity: 0;} 
to {opacity: 1;}
}
@keyframes fadeIn {
from {opacity: 0;}
to {opacity:1 ;}
}
</style>

<section class="inner_content dicovercont whitebg">
<div class="wid">

@php
//$data = $discdet;
//echo '<pre>';
//print_r($discdet);
//exit;

$i=0;
 
@endphp
<?php $x = 1; ?>
@foreach($discdet as $discdata)

@php
$i++;
@endphp

<div class="dicmain">
<div class="diciner spslide">
<div class="dsc1">
<h4>{{$discdata->dtitle}}</h4>
<p>
@php
//echo $discdata->dcontent;
$content = $discdata->dcontent;
$printsec = substr($content,0,400);
echo strip_tags($printsec).'...';
@endphp
</p>
<a href="{{URL::to('/'.$discdata->cmsslug)}}" class="discknmr">Know More</a>

<div class="whtsocial">
<span>Spread the taste</span>

@php
$data = $pagedet['theme'];
@endphp

@foreach($data as $datas)

@php 
$tmobile = $datas->tmobile;
$taddress = $datas->taddress;
$tphone = $datas->tphone;
$temail = $datas->temail;
$tfacebook = $datas->tfacebook;
$ttwitter = $datas->ttwitter;
$tyoutube = $datas->tyoutube;
$tinstagram = $datas->tinstagram;
$tcontent = $datas->tcontent;
@endphp

@endforeach
<ul>
<!-- <li><a href="http://www.facebook.com/sharer/sharer.php?u={{URL::to('/'.$discdata->cmsslug)}}" target="_blank"></a></li>
<li><a href="http://twitter.com/home?status=Fil+in+a+title+{{URL::to('/'.$discdata->cmsslug)}}" target="_blank"></a></li>
<li><a href="{{$tinstagram}}" target="_blank"></a></li>
<li><a href="{{$tyoutube}}" target="_blank"></a></li>
<li><a href="mailto:?subject=I wanted you to share the Dish&amp;body=Check out this Dish {{URL::to('/'.$discdata->cmsslug)}}"
   title="Share by Email"></a></li> -->

   <li> <div class="addthis_inline_share_toolbox"></div>
   <button id="myBtnmail{{$x}}" onclick="modalsmail({{$x}})" class="modl-btn-pop">mail</button>
 </li>
   
<li></li>

</ul>
<div id="myModalmail{{$x}}" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close" onclick="closemodalemail({{$x}})">&times;</span>
     <form method="post" action="{{URL::route('sharebyemail')}}"  onsubmit="return validateEnquiryPop();">
      <input type="hidden" name="_token" value="<?php echo (csrf_token()); ?>">
    <ul>
                    <li>
                        <span class="fldCvrSpn">
                        <input type="email" name="useremail" id="useremail" placeholder="Your Email">
                         <span class='help-block error' id="popeemail_error" style="display: none"></span>
                        </span>
                    </li>
                    <li>
                        <span class="fldCvrSpn">
                        <input type="email" name="frdemail" id="frdemail" placeholder="Friend Email">
                         <span class='help-block error' id="popfrdeemail_error" style="display: none"></span>
                        </span>
                    </li>
                    
                      <li>
                        <span class="fldCvrSpn">
                         <input title="Refer" type="text" name="refer" id="refer"  placeholder="Refer" value="{{URL::to('/'.$discdata->cmsslug)}}" readonly>
                          <span class='help-block error' id="refer_error" style="display: none"></span>
                        </span>
                        </li>

                    <li>
                        <span class="fldCvrSpn">
                        <textarea name="note" id="note" placeholder="Note"></textarea>
                        <span class='help-block error' id="note_error" style="display: none"></span>
 
                        </span>

                        </li>
                    
                    <li>
                    <input type="submit" name="button" id="button" value="Share Now">
                    </li>
                </ul>
              </form>
  </div>

</div>
</div>
</div>

<div class="disc2 @if($i%2==0) {{'rteside'}} @endif">
<div class="disciner">
								@if($discdata->dbanner!='' && $discdata->dvideo=='')
								   <img src="{{URL::asset('public/images/dishes/'.$discdata->dbanner)}}" />
                                @endif
                                @if($discdata->dvideo!='')


<!-- Trigger/Open The Modal -->
<img src="{{URL::asset('public/images/dishes/'.$discdata->dbanner)}}"><button id="myBtn{{$x}}" onclick="modals({{$x}})" class="modl-btn"><img src="{{URL::asset('public/images/dishes/video.png')}}"></button>

<!-- The Modal -->
<div id="myModal{{$x}}" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close" onclick="closemodal({{$x}})">&times;</span>
    <video width="500" controls>
   <source src="{{asset('public/images/dishes/'.$discdata->dvideo)}}" type="video/webm">
                <source src="{{asset('public/images/dishes/'.$discdata->dvideo)}}" type="video/mp4">
</video>
  </div>

</div>


            @endif
                               
</div>
</div>

</div>
</div>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5c8b8dd9f7762ca4"></script>






<?php $x = $x + 1; ?>

@endforeach
</div>
</div>
</section>



<section class="subtrecp">
<div class="wid">
<div class="res1"><img src="{{asset('public/frontend/images/submitres1.png')}}"></div>
<div class="res2"><img src="{{asset('public/frontend/images/submitres2.png')}}"></div>
<div class="res3"><img src="{{asset('public/frontend/images/submitres3.png')}}"></div>


<h3>Submit Your Recipe</h3>
<a href="{{$pagedet['urlSlugs'][23]}}">Submit</a>

</div>
</section>





<script>
function modals(i) {

var modal = document.getElementById('myModal'+i);

// Get the modal

// Get the button that opens the modal
var btn = document.getElementById("myBtn"+i);

// Get the <span> element that closes the modal
// var span = document.getElementsByClassName("close"+i)[0];

// When the user clicks the button, open the modal 
//btn.onclick = function() {
    modal.style.display = "block";
//}

// When the user clicks on <span> (x), close the modal
// span.onclick = function() {
//     modal.style.display = "none";
// }

// When the user clicks anywhere outside of the modal, close it
  window.onclick = function(event) {
      if (event.target == modal) {
           modal.style.display = "none";
       }
  }
}

function closemodal(i){
var modal = document.getElementById('myModal'+i);

    modal.style.display = "none";
     $('video').trigger('pause');


}

window.onclick = function(event) {
  var modal = document.getElementsByClassName("modal");

       $('.modal').hide();
      if (event.target == modal) {
           modal.style.display = "none";
       }
  }
</script>



<script>
function modalsmail(i) {

var modal = document.getElementById('myModalmail'+i);

// Get the modal

// Get the button that opens the modal
var btn = document.getElementById("myBtnmail"+i);

// Get the <span> element that closes the modal
// var span = document.getElementsByClassName("close"+i)[0];

// When the user clicks the button, open the modal 
//btn.onclick = function() {
    modal.style.display = "block";
//}

// When the user clicks on <span> (x), close the modal
// span.onclick = function() {
//     modal.style.display = "none";
// }

// When the user clicks anywhere outside of the modal, close it
  window.onclick = function(event) {
      if (event.target == modal) {
           modal.style.display = "none";
       }
  }
}

function closemodalemail(i){
var modal = document.getElementById('myModalmail'+i);

    modal.style.display = "none";
     $('video').trigger('pause');


}

window.onclick = function(event) {
  var modal = document.getElementsByClassName("modal");

       $('.modal').hide();
      if (event.target == modal) {
           modal.style.display = "none";
       }
  }


</script>

<script src="http://code.jquery.com/jquery-3.1.1.slim.min.js"></script>

@endsection

<script type="text/javascript">
    function validateEnquiryPop(){


    var flag=0;


 //alert($("#useremail").val());

    if ($("#useremail").val() == ""){
      $("#popeemail_error").removeClass("htitlee");
       $("#popeemail_error").addClass("error");
      $("#popeemail_error").show();
      $("#popeemail_error").html("Please enter Email");
      flag=1;


   
    }
    if($("#useremail").val()){
        var email=$("#useremail").val();
        var pattern = /^[a-zA-Z0-9\-_]+(\.[a-zA-Z0-9\-_]+)*@[a-z0-9]+(\-[a-z0-9]+)*(\.[a-z0-9]+(\-[a-z0-9]+)*)*\.[a-z]{2,4}$/;
        if (pattern.test(email)) {


        } else {
        $("#popeemail_error").removeClass("htitlee");
        $("#popeemail_error").addClass("error");
        $("#popeemail_error").show();
        $("#popeemail_error").html("Please enter a valid email");
        flag=1;
          
        }
     }
     
    if ($("#frdemail").val() == ""){
      $("#popfrdeemail_error").removeClass("htitlee");
       $("#popfrdeemail_error").addClass("error");
      $("#popfrdeemail_error").show();
      $("#popfrdeemail_error").html("Please enter Email");
      flag=1;
     
    }
    if($("#frdemail").val()){
        var email=$("#frdemail").val();
        var pattern = /^[a-zA-Z0-9\-_]+(\.[a-zA-Z0-9\-_]+)*@[a-z0-9]+(\-[a-z0-9]+)*(\.[a-z0-9]+(\-[a-z0-9]+)*)*\.[a-z]{2,4}$/;
        if (pattern.test(email)) {


        } else {
        $("#popfrdeemail_error").removeClass("htitlee");
        $("#popfrdeemail_error").addClass("error");
        $("#popfrdeemail_error").show();
        $("#popfrdeemail_error").html("Please enter a valid email");
        flag=1;
       
        }
     }  



    if($("#refer").val() == ""){
             
        $("#refer_error").removeClass("htitlee");
        $("#refer_error").addClass("error");
        $("#refer_error").show();
        $("#refer_error").html("Please enter input");
        flag=1;
       

    }

    
    if(flag==1)
      return false;
    else
     
      return true;
  }


</script>
</script>