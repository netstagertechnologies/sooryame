<!DOCTYPE HTML>
<html lang="{{ app()->getLocale() }}">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<meta name="format-detection" content="telephone=no"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=1.0, minimum-scale=1.0, maximum-scale=1.0">
<meta property="og:site_name" content="Soorya"/>
<script>
  function myFunction() {
  setTimeout(function(){
        $("#aaa").addClass("info-text-active");
    }, 4000);
  }
  </script>
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-147980348-1"></script>
  <script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-147980348-1');
  </script>
@if(Request::segment(1) == '')
<meta property="og:title" content="Get Traditional Stone Grinded Batter Mix, Soorya"/>
<meta property="og:description" content="Get Fine Quality and Cheapest & traditional stone grinded Idli / Dosa / Uttapam / Masala Dosa batter mix in Dubai, UAE. Call +971502104895"/>
<title>Get Traditional Stone Grinded Idli Dosa Maavu Batter Mix Dubai UAE, Soorya</title>
@elseif(Request::segment(1) == 'know-us')
<meta property="og:title" content="Get Tasty Batters with No Preservative, Soorya"/>
<meta property="og:description" content="Find the top quality batter mix with no preservatives added. Check with +971502104895 to find more info."/>
<title>Soorya - Get Tasty Batters with No Preservative</title>
@elseif(Request::segment(1) == 'cook-book')
<meta property="og:title" content="Know the Batter Secrets of Soorya from our Cookbook"/>
<meta property="og:description" content="Learn more about the Batter secrets and find in detail from the Cookbook, Call +971502104895 "/>
<title>Soorya - Know the Batter Secrets of Soorya from our Cookbook</title>
@elseif(Request::segment(1) == 'discoveries')
<meta property="og:title" content="Discover the Benefits on Non preserved Batters, Soorya"/>
<meta property="og:description" content="Check and Learn in detail on No Preservative added recipes secrets of Soorya, Call +971502104895"/>
<title>Soorya - Discover the Benefits on Non preserved Batters</title>
@elseif(Request::segment(1) == 'contest')
<meta property="og:title" content="Get a Chance to win with Soorya Cooking Sides"/>
<meta property="og:description" content="Put a twist with your recipes into Soorya Cooking contest, Learn more about the Contest, Dial +971502104895"/>
<title>Soorya - Get a Chance to win with Soorya Cooking Sides</title>
@elseif(Request::segment(1) == 'here-to-serve-You')
<meta property="og:title" content="Vote for our Batter on your experience"/>
<meta property="og:description" content="Tell us your experience regarding the batter we provided of idli/dosa/uttapam, Vote your feedback at the portal provided."/>
<title>Soorya - Vote for our Batter on your experience</title>
@elseif(Request::segment(1) == 'contact')
<meta property="og:title" content="Check with us to get your Choice"/>
<meta property="og:description" content="Got any suggestions or want to tell about the experience on batters we provided, Call +971502104895."/>
@else
{{-- <title>{{ config('app.name', 'Soorya') }}</title> --}}
<title>{{ Seo::get('title')}}</title>
@endif

<meta name="description" content="{{ Seo::get('description') }}" />
{{-- <meta name="title"  content="{{ Seo::get('title') }}" /> --}}
<meta name="keyword" content="{{ Seo::get('keywords') }}" />
<link rel="icon" type="image/png" sizes="32x32" href="{{asset('public/frontend/images/favicon32.png')}}">
<link rel="icon" type="image/png" sizes="96x96" href="{{asset('public/frontend/images/favicon96.png')}}">
<link rel="icon" type="image/png" sizes="16x16" href="{{asset('public/frontend/images/favicon16.png')}}">

<meta name="csrf-token" content="{{ csrf_token() }}">

<!--==== FONTS ======-->
<!-- <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet"> -->
<link href="https://fonts.googleapis.com/css?family=Love+Ya+Like+A+Sister" rel="stylesheet">

<link href="{{asset('public/frontend/favicon.ico')}}" rel="shortcut icon" type="image/x-icon" />
<link href="{{asset('public/frontend/style.css')}}" rel="stylesheet" type="text/css">
</head>

<?php if(Request::segment(1) == ''){ ?>
<body onload="myFunction()">
 <div class="fullWidth">
 <div class="wid">

<div class="footthre" id="aaa">
<img src="{{asset('public/frontend/images/anmi.png')}}" alt="anmi" title="anmi"></div>
 </div>
 </div>

<?php } else { ?>
<body>
<?php } ?>

@php
$data = $pagedet['theme'];
@endphp

@foreach($data as $datas)

@php
$tmobile = $datas->tmobile;
$taddress = $datas->taddress;
$tphone = $datas->tphone;
$temail = $datas->temail;
$tfacebook = $datas->tfacebook;
$ttwitter = $datas->ttwitter;
$tyoutube = $datas->tyoutube;
$tinstagram = $datas->tinstagram;
$tcontent = $datas->tcontent;
@endphp

@endforeach

<!-- @if(Request::is('/'))
<div id="preloader">
<div id="status"></div>
</div>
@endif -->
<header>
<div class="wid">
<div class="tophed">
<a class="phOne" href="tel:{{$tphone}}">{{$tphone}}</a>
<div class="logo"><a href="{{URL::to('/')}}"><img src="{{asset('public/frontend/images/soorya_logo.png')}}" alt="soorya_logo" title="soorya_logo"></a></div>
<a class="mAil" href="mailto:{{$temail}}">{{$temail}}</a>
</div>

<div class="menuD">
<a href="#menu" id="nav-toggle" class="menu-link">
<span></span>
</a>
<nav id="menu" class="menu">
<ul class="level-01">
<li  @if(Request::is('/')) class="current_page_item current-menu-item" @endif><a href="{{URL::to('/')}}" title="Home">Home</a></li>
<li @if(Request::is($pagedet['urlSlugs'][7])) class="current_page_item current-menu-item" @endif><a href="{{$pagedet['urlSlugs'][7]}}" title="Know Us">Know Us</a><span class="has-subnav">&#x25BC;</span>

</li>
<li @if(Request::is($pagedet['urlSlugs'][9])) class="current_page_item current-menu-item" @endif><a href="{{$pagedet['urlSlugs'][9]}}" title="Cook Book ">Cook Book </a></li>
<li @if(Request::is($pagedet['urlSlugs'][15])) class="current_page_item current-menu-item" @endif><a href="{{$pagedet['urlSlugs'][15]}}" title="Discoveries">Discoveries</a></li>
<li @if(Request::is($pagedet['urlSlugs'][23])) class="current_page_item current-menu-item" @endif><a href="{{$pagedet['urlSlugs'][23]}}" title="Contest">Contest</a></li>
<li @if(Request::is($pagedet['urlSlugs'][29])) class="current_page_item current-menu-item" @endif><a href="{{$pagedet['urlSlugs'][29]}}" title="Poll">Poll</a></li>
<li @if(Request::is($pagedet['urlSlugs'][28])) class="current_page_item current-menu-item" @endif><a href="{{$pagedet['urlSlugs'][28]}}" title="Contact">Contact</a></li>
</ul>
</nav>
</div>
</div>
</header>


@if(Session::has('message'))
<div class="ssn_msg">
<div id="messageModal" class="modal fade">
  <div class="modal-content">
  <div class="mc_clz">

      <span onclick="closeMessageModal()"
      class="close">&times;</span>
      {{Session::get('message')}}
    </div>
  </div>
</div>
</div>

@endif

 @yield('content')







<footer>
<div class="wid">
<div class="foot1">

<div class="foothed">SUBSCRIBE NEWSLETTER</div>
<p>Subscribe to our Email Newsletter for our Mouth watering batter mix updates and information to your inbox.</p>

<form method="post" action="{{URL::route('newssave')}}" id="newssave" onsubmit="return validateNSForm()">
<input type="hidden" name="_token" value="{{csrf_token()}}">
<ul>
<li>
  <input type="text" placeholder="Enter your email" id="newsemail" name="newsemail">
</li>
<li><input type="submit" class="newsub" value="submit" id="nwsemlsub"></li>
</ul>
<div class="newsmsg" id="newsmsg"></div>
</form>
</div>

<div class="foot2">
<div class="foothed">know us</div>
<p>{{$tcontent}}</p>
</div>

<div class="foot3">
<div class="foothed">contact us</div>
<ul class="adrs">
<li class="mpr"><?php echo $taddress; ?></li>

<li class="phne"><a href="tel:{{$tmobile}}">{{$tmobile}}</a></li>
<li class="mailSP"><a href="mailto:{{$temail}}">{{$temail}}</a></li>

</ul>

<ul class="socilmeda">
<li class="fb"><a href="{{$tfacebook}}" target="_blank"></a></li>
<li class="tw"><a href="{{$ttwitter}}" target="_blank"></a></li>
<li class="insta"><a href="{{$tinstagram}}" target="_blank"></a></li>
<li class="yt"><a href="{{$tyoutube}}" target="_blank"></a></li>


</ul>

</div>



<div class="footerlogosc">
<div class="auto"><img src="{{asset('public/frontend/images/auto.png')}}" alt="auto" title="auto"></div>

<?php if(Request::segment(1) == ''){ ?>
<div class="footfiximg left">
<img src="{{asset('public/frontend/images/dosafoot.png')}}" alt="dosafoot" title="dosafoot" data-scroll="toggle(.fromTopIn, .fromTopOut)"><img src="{{asset('public/frontend/images/mamifoot.png')}}" alt="mamifoot" title="mamifoot" data-scroll="toggle(.fromBottomIn, .fromBottomOut)"><img src="{{asset('public/frontend/images/iddlifoot.png')}}" alt="iddlifoot" title="iddlifoot" data-scroll="toggle(.fromTopIn, .fromTopOut)">
  </div>
<?php } ?>

<div class="foterlogo"><a href="{{URL::to('/')}}"><img src="{{asset('public/frontend/images/soorya_logo.png')}}" alt="soorya_logo1" title="soorya_logo1"></a></div>

<?php if(Request::segment(1) == ''){ ?>

<div class="footfix">
<div class="footfiximg">
<img src="{{asset('public/frontend/images/dosafoot.png')}}" alt="dosafoot1" title="dosafoot1" data-scroll="toggle(.fromTopIn, .fromTopOut)"><img src="{{asset('public/frontend/images/mamifoot.png')}}" alt="mamifoot1" title="mamifoot1" data-scroll="toggle(.fromBottomIn, .fromBottomOut)"><img src="{{asset('public/frontend/images/iddlifoot.png')}}" alt="iddlifoot1" title="iddlifoot1" data-scroll="toggle(.fromTopIn, .fromTopOut)">
</div>
<div class="lory"><img src="https://sooryame.com/public/frontend/images/lory.png" alt="lory1" title="lory1"></div>
</div>
<?php } else { ?>
<div class="lory"><img src="{{asset('public/frontend/images/lory.png')}}" alt="lory" title="lory"></div>
<?php } ?>
<p>Probably the Healthiest Idlis & Dosas in Town</p>


<ul class="ftrmenu">
<li><a href="{{URL::to('/')}}" title="home">Home </a></li>
<li><a href="{{$pagedet['urlSlugs'][7]}}"  title="Know us"> Know Us </a></li>
<li><a href="{{$pagedet['urlSlugs'][9]}}" title="Cook Book">Cook Book</a></li>
<li><a href="{{$pagedet['urlSlugs'][15]}}" title="Discoveries">Discoveries</a></li>
<li><a href="{{$pagedet['urlSlugs'][23]}}" title="contest">Contest </a></li>
<li><a href="{{$pagedet['urlSlugs'][29]}}" title="Poll">Poll </a></li>
<li><a href="{{$pagedet['urlSlugs'][28]}}" title="Contact">Contact</a></li>
<li><a href="{{URL::to('/sitemap')}}" title="Sitemap">Sitemap</a></li>
</ul>

</div>





</div>
<div class="ftrbdr"></div>
<div class="cpy">
<p> © {{date('Y')}} Soorya, All rights reserved.</p>
</div>
</footer>


<!--Menu-->

<script type="text/javascript" src="{{asset('public/frontend/js/jquery.min.js')}}"></script>

<script>

  function hide_err(obj){
        $(obj).hide();
    }


function validateNSForm(){

  var newsemail = $('#newsemail').val();

  if(newsemail=='') {
    var erobj = $("#newsemail");
            var msg = "Please Enter Email Address";
            erobj.parent().append('<span class="errCls" onmouseover="hide_err(this);">'+msg+'</span>');
            return false;
  }
}

</script>
<script>
$(document).ready(function() {

  // Fakes the loading setting a timeout
    setTimeout(function() {
        $('body').addClass('loaded');
    }, 5000);

});
</script>
<script>

$('#messageModal').modal({backdrop:'static', keyboard:false});

  $(document).ready(function() {
  @if(Session::has('message'))
   // var x = "{{Session::get('message')}}";
    $("#messageBtn").trigger('click');
  @else
  $("#messageModal").hide();
  @endif


});

function mesagemodal() {

var modal = document.getElementById('messageModal');


var btn = document.getElementById("messageBtn");

    modal.style.display = "block";

  window.onclick = function(event) {
      if (event.target == modal) {
           modal.style.display = "none";
       }
  }
}

function closeMessageModal() {

    var modal = document.getElementById('messageModal');
location.reload();
    modal.style.display = "none";
}



window.onclick = function(event) {
  var modal = document.getElementsByClassName("modal");

       $('.modal').hide();
      if (event.target == modal) {
           modal.style.display = "none";
       }
  }
</script>



<script src="{{asset('public/frontend/js/jquery.easing.min.js')}}"></script>
<script src="{{asset('public/frontend/js/jquery.barChart.js')}}"></script>
<script>

 jQuery('.barChart').barChart({easing: 'easeOutQuart'});

</script>


<script>
$(window).on('load', function() { // makes sure the whole site is loaded
  $('#status').fadeOut(); // will first fade out the loading animation
  $('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
  $('body').delay(350).css({'overflow':'visible'});
});
</script>




<!-- SLIDER REVOLUTION 4.x SCRIPTS  -->

<script type="text/javascript" src="{{asset('public/frontend/rs-plugin/js/jquery.themepunch.plugins.min.js')}}"></script>
<script type="text/javascript" src="{{asset('public/frontend/rs-plugin/js/jquery.themepunch.revolution.min.js')}}"></script>

<!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
<link rel="stylesheet" type="text/css" href="{{asset('public/frontend/css/extralayers.css')}}" media="screen" />
<link rel="stylesheet" type="text/css" href="{{asset('public/frontend/rs-plugin/css/settings.css')}}" media="screen" />

<script type="text/javascript">

jQuery(document).ready(function() {
jQuery('.tp-banner').show().revolution(
{
dottedOverlay:"none",
delay:10000,
startwidth:1170,
startheight:700,
hideThumbs:200,

thumbWidth:100,
thumbHeight:50,
thumbAmount:5,

navigationType:"bullet",     // use none, bullet or thumb
navigationArrows:"none",     // nexttobullets, solo (old name verticalcentered), none
navigationStyle:"round",     // round, square, navbar, round-old, square-old, navbar-old


touchenabled:"on",
onHoverStop:"on",

swipe_velocity: 0.7,
swipe_min_touches: 1,
swipe_max_touches: 1,
drag_block_vertical: false,

parallax:"mouse",
parallaxBgFreeze:"on",
parallaxLevels:[7,4,3,2,5,4,3,2,1,0],

keyboardNavigation:"on",

navigationHAlign:"center",   // left,center,right
navigationVAlign:"bottom",   // top,center,bottom
navigationHOffset:0,   // offset position from aligned position
navigationVOffset:0,   // offset position from aligned position

soloArrowLeftHalign:"left",
soloArrowLeftValign:"center",
soloArrowLeftHOffset:20,
soloArrowLeftVOffset:0,

soloArrowRightHalign:"right",
soloArrowRightValign:"center",
soloArrowRightHOffset:20,
soloArrowRightVOffset:0,

shadow:0,
fullWidth:"on",
fullScreen:"off",

spinner:"spinner4",

stopLoop:"off",
stopAfterLoops:-1,
stopAtSlide:-1,

shuffle:"off",

autoHeight:"off",
forceFullWidth:"off",



hideThumbsOnMobile:"off",
hideNavDelayOnMobile:1500,
hideBulletsOnMobile:"off",
hideArrowsOnMobile:"on",
hideThumbsUnderResolution:0,

hideSliderAtLimit:0,
hideCaptionAtLimit:0,
hideAllCaptionAtLilmit:0,
startWithSlide:0,
videoJsPath:"rs-plugin/videojs/",
fullScreenOffsetContainer: ""
});
}); //ready
</script>



<script src="{{asset('public/frontend/js/toggle-nav.js')}}" type="text/javascript"></script>
<script>
document.querySelector( "#nav-toggle" ).addEventListener( "click", function() {
this.classList.toggle( ".active" );
});
</script>
<!--Menu Ends-->

<script type="text/javascript">
$(window).scroll(function() {
$("header").toggleClass("aniPos", $(this).scrollTop() > 0);
});
</script>
<!--Slider-->
<script src="{{asset('public/frontend/js/jquery.lightSlider.js')}}"></script>
<script>
$(document).ready(function() {
$('#bannerSlider').lightSlider({
item:1,
auto: true,
loop: true,
slideMove:1,
mode: 'slide',
addClass: 'bannerSlider',
easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
speed:1000,
pause: 3000,
adaptiveHeight:true,
responsive : [
{
breakpoint:800,
settings: {
item:1,
slideMargin:6,
}
},
{
breakpoint:480,
settings: {
item:1,
slideMove:1
}
}
]
});
});
</script>
<!--Slider Ends-->

<script type='text/javascript' src='{{asset('public/frontend/animation/ScrollTrigger.min.js')}}'></script>

<script>
window.counter = function () {
// this refers to the html element with the data-scroll-showCallback tag
var span = this.querySelector('span');
var current = parseInt(span.textContent);
span.textContent = current + 1;
};
document.addEventListener('DOMContentLoaded', function () {
var trigger = new ScrollTrigger({
addHeight: true
});
});
</script>

</body>
</html>
