@extends('layouts.app')
@section ('content')

@php
$data = $pagedet['slugData'];
//echo '<pre>';
//print_r($data);
//exit; 
@endphp

@foreach($data as $datas)

<section class="inner_page knowus" style="background-image:url({{asset('public/frontend/images/banner_bg.jpg')}});">
<div class="wid">

<h1 class="inercmnhed">{{$datas->title}}</h1>
<div class="inerbanimg"><img src="{{URL::asset('public/images/slider/'.$datas->banner)}}"alt="bannerimg" title="bannerimg"></div>

</div>
</section>

<section class="inner_content knowcont">
<div class="wid">

@php 
echo $datas->content;
@endphp

<div class="whychsc">
<h1 class="cmnheding">Why Choose Us ?</h1>
<ul>
<li>
<img src="{{asset('public/frontend/images/quality.png')}}"alt="quality" title="quality">
<span>Quality Products</span>
</li>
<li>
<img src="{{asset('public/frontend/images/no_preservation.png')}}"alt="no_preservation" title="no_preservation">
<span>No Preservatives<br>
Added</span>
</li>
<li>
<img src="{{asset('public/frontend/images/traditional.png')}}"alt="traditional" title="traditional">
<span>Traditionally Made</span>
</li>

</ul>


</div>
</div>
</section>


<section class="sec6">
<div class="wid">
<h4 class="cmnheding">Features</h4>
<ul class="featurs">
<li><img src="{{asset('public/frontend/images/features1.png')}}"alt="features1" title="features1"></li>
<li><img src="{{asset('public/frontend/images/features2.png')}}"alt="features2" title="features2"></li>
<li><img src="{{asset('public/frontend/images/features3.png')}}"alt="features3" title="features3"></li>
<li><img src="{{asset('public/frontend/images/features4.png')}}"alt="features4" title="features4"></li>
<li><img src="{{asset('public/frontend/images/emirates.png')}}"alt="emirates" title="emirates"></li>
<li><img src="{{asset('public/frontend/images/thr-seven.png')}}"alt="thr-seven" title="thr-seven"></li>
<li><img src="{{asset('public/frontend/images/vegon.png')}}"alt="vegon" title="vegon"></li>
<li><img src="{{asset('public/frontend/images/vegon2.png')}}"alt="vegon2" title="vegon2"></li>
</ul>
</div>
</section>


@endforeach

@endsection