@extends('layouts.app')

@section ('content')
<section class="inner_content cookboksplmain cookbokiner">
<div class="wid">
<div class="Cook">

<div class="sideoverlay">
    <h1>Sitemap</h1>
 </div>

</div>
</div>

</section>

<section class="inner_content cookboksplmain">
        <div class="wid">
        <ul class="listing">
            <li><a href="https://sooryame.com/" title="Home">Home</a></li>
        @foreach($pages as $cookdata)
        @if($cookdata->tableid=='0')
        <li><a href="{{URL::to('/'.$cookdata->cmsslug)}}" title="{{$cookdata->cmsslug}}">{{$cookdata->cmsslug}}</a></li>
        @if($cookdata->cmstemplate=='4')
        <ul class="sub-menu" >
        @foreach($sub as $sub1)
        <li><a href="{{URL::to('/'.$sub1->cmsslug)}}" title="{{$sub1->cmsslug}}">{{$sub1->cmsslug}}</a></li>
        @endforeach
        </ul>
        @endif
        @if($cookdata->cmstemplate=='6')
        <ul class="sub-menu" >
        @foreach($sub11 as $sub2)
        <li><a href="{{URL::to('/'.$sub2->cmsslug)}}" title="{{$sub2->cmsslug}}">{{$sub2->cmsslug}}</a></li>
        @endforeach
        </ul>
        @endif
       @endif
        @endforeach
        </ul>


         </ul>
       </div>
        </section>

@endsection
