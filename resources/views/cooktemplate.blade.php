@extends('layouts.app')

@section ('content')

@php
$data = $pagedet['slugData'];
//echo '<pre>';
//print_r($data);
//exit;
@endphp

@foreach($data as $datas)


<section class="inner_page cookbook" style="background-image:url({{asset('public/frontend/images/banner_bg.jpg')}});">
<div class="wid">
<h1 class="inercmnhed">{{$datas->title}}</h1>
<div class="inerbanimg"><img src="{{URL::asset('public/images/slider/'.$datas->banner)}}" alt="bannerimg" title="bannerimg"></div>
</div>
</section>

@endforeach

<section class="inner_content cookboksplmain">
<div class="wid">
<ul class="cookboksp">
@php
/*echo '<pre>';
print_r($cookdet);
exit; */
@endphp

@foreach($cookdet as $cookdata)
<li><a href="{{URL::to('/'.$cookdata->cmsslug)}}">
<div class="cookprts">
<img src="{{URL::asset('public/images/dishes/'.$cookdata->cbrecipeimage)}}" alt="recipe" title="recipe">
</div>
<span>{{str_replace('-', ' ', urldecode($cookdata->cbrecipename))}}</span>
</a></li>
@endforeach

</ul>

<!--<ul class="pagination">
<li class="active"><a href="">1</a></li>
<li><a href="">2</a></li>
<li><a href="">3</a></li>
<li><a href="">4</a></li>
<li><a href="">5</a></li>

</ul>-->

</div>
</section>



<section class="subtrecp">
<div class="wid">
<div class="res1"><img src="{{asset('public/frontend/images/submitres1.png')}}" alt="submitres1" title="submitres1"></div>
<div class="res2"><img src="{{asset('public/frontend/images/submitres2.png')}}" alt="submitres2" title="submitres2"></div>
<div class="res3"><img src="{{asset('public/frontend/images/submitres3.png')}}" alt="submitres3" title="submitres3"></div>


<h3>Submit Your Recipie</h3>
<a href="{{$pagedet['urlSlugs'][23]}}">Submit</a>

</div>
</section>





@endsection
