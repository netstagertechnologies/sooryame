@extends('layouts.app')
@section ('content')

<link href="{{asset('public/frontend/css/kanimate.css')}}" rel="stylesheet" type="text/css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<style>


</style>
<div id="">

<div id="loader-wrapper">
<div id="loader" style="background-image:url(https://sooryame.com/public/frontend/images/soorya.gif)"></div>

<div class="loader-section section-left"></div>
<div class="loader-section section-right"></div>

</div>

<section class="banner">
<article class="">
<div class="tp-banner-container">
<div class="tp-banner" >
<ul>


<!-- SLIDE  -->
<li data-transition="zoomin" data-slotamount="7" data-masterspeed="1500" data-thumb="homeslider_thumb4.jpg"  data-saveperformance="on"  data-title="Mobile Interaction">

<!-- LAYERS -->
<!-- LAYER NR. 1 -->

<div class="tp-caption tp-caption black_thin_34 customin tp-resizeme rs-parallaxlevel-0 sliitem1"
data-x="128"
data-y="273"
data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
data-speed="1000"
data-start="2000"
data-easing="Power4.easeInOut"
data-elementdelay="0.1"
data-endelementdelay="0.1"
data-endspeed="300"
style="z-index: 3;"><div data-easing="Power4.easeInOut"
data-speed="0.5"
data-zoomstart="0.75"
data-zoomend="1">
<img data-lazyload="{{asset('public/frontend/images/banner_border.png')}}" alt="bannerimg1" title="bannerimg1" src="#">
</div>
</div>
<!-- LAYER NR. 2 -->
<div class="tp-caption large_text sliitem2"
data-x="0"
data-y="344"
data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
data-speed="1000"
data-start="2500"
data-easing="Power4.easeInOut"
	data-splitin="chars"
			data-splitout="none"
data-elementdelay="0.1"
data-endelementdelay="0.1"
data-endspeed="300"
style="z-index: 3;"><div data-easing="Power4.easeInOut"
data-speed="0.5"
data-zoomstart="0.75"
data-zoomend="1">
Ethnic Indian Taste
</div>
</div>

<!-- LAYER NR. 3 -->
<div class="tp-caption medium_text sliitem3"
data-x="-17"
data-y="406"
data-speed="1000"
data-start="3000"
data-easing="Power3.easeInOut"
data-splitin="none"
data-splitout="none"
data-elementdelay="0.1"
data-endelementdelay="0.1"
data-endspeed="300"
style="z-index: 6; max-width: auto; max-height: auto; white-space: nowrap;"><h1 class="dosa-tag"> Traditional Stone Grinded Idli/Dosa/Uttapam Batter Mix
  </h1></div>



<!-- LAYER NR. 4 -->
<div class="tp-caption tp-resizeme sliitem4"
data-x="160"
data-y="497"
data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
data-speed="1000"
data-start="3500"
data-easing="Power3.easeInOut"
data-splitin="none"
data-splitout="none"
data-elementdelay="0.1"
data-endelementdelay="0.1"
data-endspeed="600"
style="z-index: 8; max-width: auto; max-height: auto; white-space: nowrap;"><a href="{{$pagedet['urlSlugs'][7]}}">Know More</a>
</div>



<!-- LAYER NR. 5 -->
<div class="tp-caption sfr fadeout rs-parallaxlevel-5 sliitem5"
data-x="right" data-hoffset="-70"
data-y="top" data-voffset="195"
data-speed="1000"
data-start="1500"
data-easing="Power1.easeInOut"
data-elementdelay="0.1"
data-endelementdelay="0.1"
data-endspeed="300"
style="z-index: 2;"><img src="#" data-lazyload="{{asset('public/frontend/images/banner_soorya.png')}}" alt="bannerimgs" title="bannerimgs">
</div>
<!-- LAYER NR. 6 -->

<div class="tp-caption sfr fadeout sliitem6"
data-x="right" data-hoffset="-60"
data-y="top" data-voffset="115"
data-speed="1000"
data-start="1500"
data-easing="Power3.easeInOut"
data-splitin="none"
data-splitout="none"
data-elementdelay="0.1"
data-endelementdelay="0.1"
data-endspeed="300"
style="z-index: 6; max-width: auto; max-height: auto; white-space: nowrap;"><img src="#" alt="soorya_veg" data-lazyload="{{asset('public/frontend/images/soorya_veg.png')}}">
</div>


</li>
<li data-transition="zoomout" data-slotamount="1" data-masterspeed="1500" data-thumb="homeslider_thumb5.jpg"  data-saveperformance="on"  data-title="Fixed-Size Banner">

<!-- LAYERS -->

<!-- LAYER NR. 1 -->
<img src="{{asset('public/frontend/images/banner02.png')}}" alt="banner2" title="banner2">
</li>
@include('banneradds')



</ul>
<div class="tp-bannertimer"></div>	</div>
</div>
</article>
</section>

<button onclick="mesagemodal()" class="button" id="messageBtn" style="display: none">Open Modal</button>

@if(Session::has('message'))
<div class="ssn_msg">
<div id="messageModal" class="modal">
  <div class="modal-content">
  <div class="mc_clz">

      <span onclick="closeMessageModal()"
      class="close">&times;</span>
      {{Session::get('message')}}
    </div>
  </div>
</div>
</div>
@endif

<section class="sec1">
<div class="wid">
@foreach($pgdet as $page)
@if($page->pid==13)
@php
echo $page->content;
@endphp
@endif
@endforeach
 <div class="plates">
 <ul>
 <li><img src="{{asset('public/frontend/images/plate1.png')}}" alt="plate1" title="plate1"></li>
 <li><img src="{{asset('public/frontend/images/plate2.png')}}" alt="plate2" title="plate2"></li>
 <li><img src="{{asset('public/frontend/images/plate3.png')}}" alt="plate3" title="plate3"></li>

 </ul>

 </div>
</div>
</section>


<section class="sec2">
<div class="bgoverlay"></div>
<div class="wid">
<h2 class="cmnheding">Cook Book</h2>

@php
$i=0;
@endphp

@foreach($cookdet as $cookdata)

@php
$i++;
@endphp

<div class="Cook">
<div class="sideoverlay">
<div class="cooksec1">
<h3><a href="{{URL::to('/'.$cookdata->cmsslug)}}" title="{{$cookdata->cbrecipename}}">{{$cookdata->cbrecipename}}</a></h3>
@php
//echo $cookdata->cbrecipecontent;
$content = $cookdata->cbrecipecontent;
echo substr($content,0,467);
//echo substr($content,0,700);
//echo $pos;
@endphp
</div>
<div class="cooksec2 @if($i%2==0) {{'rteside'}} @endif">
<img src="{{URL::asset('public/images/dishes/'.$cookdata->cbrecipeimage)}}" alt="cbrecipeimage">

</div>
</div>
</div>

@endforeach


<ul class="cmnbtns">
<li><a href="{{$pagedet['urlSlugs'][9]}}">View More</a></li>
<li><a href="{{$pagedet['urlSlugs'][23]}}">Submit Your Recipe</a></li>
</ul>
</div>

</section>



<section class="sec3">
<div class="wid">
<div class="contestcontent">
<h3>Soorya
<span>Cooking Contest</span></h3>
<a href="{{$pagedet['urlSlugs'][23]}}">Know More</a>
</div>
<div class="cookman"><img src="{{asset('public/frontend/images/soorya_cook.gif')}}" alt="cook1" title="cook1"></div>
</div>
</section>


@foreach($polldet as $poll)

<section class="sec4">
<div class="servimg"><img src="{{asset('public/frontend/images/serve.png')}}" alt="serve" title="serve"></div>
<div class="hapyyou">
<h3 class="cmnheding">Here to Serve You</h3>
@php
echo $poll->pollcontent;
@endphp


<form method="post" action="{{URL::route('pollsave')}}" id="savepoll">

{{csrf_field()}}

  <p>
    <input type="radio" id="test1" name="pollcheck" value="4" checked>
    <label for="test1">Excellent</label>
  </p>
  <p>
    <input type="radio" id="test2" name="pollcheck" value="3">
    <label for="test2">Good</label>
  </p>
  <p>
    <input type="radio" id="test3" name="pollcheck" value="2">
    <label for="test3">Ok</label>
  </p>
   <p>
    <input type="radio" id="test4" name="pollcheck" value="1">
    <label for="test4">Adverse</label>
  </p>

<button>Vote</button>

<input type="hidden" name="hidpollid" value="{{$poll->pollid}}">

</form>

</div>
</section>

@endforeach

<section class="sec5">
<div class="wid">
<h3 class="cmnheding"> Discoveries</h3>

@php
$i=0;
@endphp
<?php $x = 1; ?>
@foreach($discdet as $discdata)

@php
$i++;
@endphp

<div class="dicmain">
<div class="diciner spslide">
<div class="dsc1">
<h4>{{$discdata->dtitle}}</h4>
@php
//echo $discdata->dcontent;
$content = $discdata->dcontent;
echo substr($content,0,400);
@endphp
</p>
<a href="{{URL::to('/'.$discdata->cmsslug)}}" class="disc_cls">Know More</a>
</div>

<div class="disc2 @if($i%2==0) {{'rteside'}} @endif">
<div class="disciner">
                @if($discdata->dbanner!='' && $discdata->dvideo=='')
                   <img src="{{URL::asset('public/images/dishes/'.$discdata->dbanner)}}" alt="dishes" title="dishes"/>
                                @endif
                                @if($discdata->dvideo!='')


<!-- Trigger/Open The Modal -->
<img src="{{URL::asset('public/images/dishes/'.$discdata->dbanner)}}" alt="dishes1" title="dishes1"><button id="myBtn{{$x}}" onclick="modals({{$x}})" class="modl-btn"><img src="{{URL::asset('public/images/dishes/video.png')}}" alt="video1"></button>

<!-- The Modal -->
<div id="myModal{{$x}}" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close" onclick="closemodal({{$x}})">&times;</span>
    <video width="500" controls >
   <source src="{{asset('public/images/dishes/'.$discdata->dvideo)}}" type="video/webm">
                <source src="{{asset('public/images/dishes/'.$discdata->dvideo)}}" type="video/mp4">
</video>
  </div>

</div>


            @endif

</div>
</div>


</div>
</div>

<?php $x = $x + 1; ?>
@endforeach



<a class="cmnbtn" href="{{$pagedet['urlSlugs'][15]}}">View All</a>
</div>
</section>

<script>




$(document).ready(function() {


  @if(Session::has('message'))

    var x = "{{Session::get('message')}}";

    $("#messageBtn").trigger('click');

  @endif

});



function modals(i) {

var modal = document.getElementById('myModal'+i);


var btn = document.getElementById("myBtn"+i);

    modal.style.display = "block";

  window.onclick = function(event) {
      if (event.target == modal) {
           modal.style.display = "none";
       }
  }
}

function closemodal(i){
var modal = document.getElementById('myModal'+i);

    modal.style.display = "none";
    $('video').trigger('pause');


}

function mesagemodal() {

var modal = document.getElementById('messageModal');


var btn = document.getElementById("messageBtn");

    modal.style.display = "block";

  window.onclick = function(event) {
      if (event.target == modal) {
           modal.style.display = "none";
       }
  }
}

function closeMessageModal() {

    var modal = document.getElementById('messageModal');

    modal.style.display = "none";
}



window.onclick = function(event) {
  var modal = document.getElementsByClassName("modal");

       $('.modal').hide();
      if (event.target == modal) {
           modal.style.display = "none";
       }
  }
</script>

@endsection
