@extends('layouts.app')

@section ('content')

@php
$data = $pagedet['slugData'];
//echo '<pre>';
//print_r($data);
//exit;
@endphp

@foreach($data as $datas)


<section class="inner_page contact whitebg" style="background-image:url({{asset('public/frontend/images/banner_bg.jpg')}});">
<div class="wid">

<h1 class="inercmnhed">{{$datas->title}}</h1>
<div class="inerbanimg"><img src="{{URL::asset('public/images/slider/'.$datas->banner)}}" alt="banner1" title="banner1"></div>

</div>
</section>

@if(Session::has('message'))

      <div class="callout callout-warning">
          <h4>{{ Session::get('message') }}</h4>
      </div>

@endif

<section class="inner_content contactiner whitebg">
<div class="wid">
<div class="contfrm">
<h2 class="cmnheding">Write to us</h2>
<form method="post" action="{{URL::route('sentcontact')}}" id="contactsent"  enctype="multipart/form-data">

<input type="hidden" name="_token" value="{{csrf_token()}}">
<ul>
<li><input type="text" placeholder="Your Name" name="contact_name" id="contact_name"><span class='help-block' id="error1" onmouseover="hide(1);"></span></li>
<li><input type="text" placeholder="Phone" name="contact_phone" id="contact_phone" onkeypress="return isNumber(event)"><span class='help-block' id="error2" onmouseover="hide(2);" ></span></li>
<li><input type="text" placeholder="Email" name="contact_email" id="contact_email"><span class='help-block' id="error3" onmouseover="hide(3);"></span></li>
<li><textarea placeholder="Message" name="contact_message" id="contact_message"></textarea></li>

</ul>
<input type="submit" class="consub" value="Submit" onclick="validation_check();return false;">
</form>


</div>

<div class="contsdrs">

@php
$themedata = $pagedet['theme'];
@endphp

@foreach($themedata as $tdatas)

@php
$tmobile = $tdatas->tmobile;
$taddress = $tdatas->taddress;
$tphone = $tdatas->tphone;
$temail = $tdatas->temail;
$tfacebook = $tdatas->tfacebook;
$ttwitter = $tdatas->ttwitter;
$tyoutube = $tdatas->tyoutube;
$tinstagram = $tdatas->tinstagram;
$tcontent = $tdatas->tcontent;
@endphp


<h2 class="cmnheding">Address</h2>
<ul class="adrs">
<li class="mpr">@php echo $taddress @endphp</li>
<li class="phne"><a href="tel:{{$tphone}}">{{$tphone}}</a></li>
<li><a href="tel:{{$tmobile}}">{{$tmobile}}</a></li>
<li class="mailSP"><a href="mailto:{{$temail}}">{{$temail}}</a></li>

</ul>
<ul class="socilmeda">
<li class="fb"><a href="{{$tfacebook}}"></a></li>
<li class="tw"><a href="{{$ttwitter}}"></a></li>
<li class="insta"><a href="{{$tinstagram}}"></a></li>
<li class="yt"><a href="{{$tyoutube}}"></a></li>


</ul>
@endforeach
<div class="contmap">
<!--<a href=""><img src="images/contactmap.png"></a>-->
@php
echo $datas->content;
@endphp
</div>

</div>


</div>
</section>

@endforeach

@endsection

<script type="text/javascript">

	function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

		function validation_check() {



			var flag=0;

			if($('#contact_name').val()=='') {
				$('#error1').show();
				$('#error1').html('Please Enter Your Name');
				goToByScroll("contact_name");
                $("#contact_name").focus();
				flag=1;
			}
			if($('#contact_phone').val()=='') {
				$('#error2').show();
				$('#error2').html('Please Enter Your Phone');
				goToByScroll("contact_phone");
                $("#contact_phone").focus();
				flag=1;
			}


			if($('#contact_email').val()=='') {
				$('#error3').show();
				$('#error3').html('Please Enter Your Email');
				goToByScroll("contact_email");
                $("#contact_email").focus();
				flag=1;
			}

				if($("#contact_email").val())
                            {
                                var x = $("#contact_email").val();
                                var atpos = x.indexOf("@");
                                var dotpos = x.lastIndexOf(".");
                                if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length)
                                   {
                                    alert("Not a valid e-mail address");
                                    flag=1;
                                   }


                            }

			if(flag==1) {
				return false;
			} else {
				$( "#contactsent" ).submit();
				return true;
			}

		}

		   function goToByScroll(id, msg) {

                            $("#"+id).parent().find('span').text(msg);
                            $("#"+id).parent().parent().parent().find('.help-block').removeClass('hide');
                            $("#"+id).parent().parent().parent().addClass('has-error');
                            $("#"+id).focus();

                            // Remove "link" from the ID
                            id = id.replace("link", "");
                            // Scroll
                            // $('html,body').animate({ scrollTop: $("#" + id).offset().top - 100}, 'slow');
                        }

            function hide(id) {

			var errid = '#error'+id;
			$(errid).hide();
			$(errid).html('');


		}

	</script>
