@extends('layouts.app')

@section ('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

@php
$data = $pagedet['slugData'];
//echo '<pre>';
//print_r($data);
//exit;
@endphp

@foreach($data as $datas)


<section class="inner_page poll whitebg" style="background-image:url({{asset('public/frontend/images/banner_bg.jpg')}});">
<div class="wid">

<h1 class="inercmnhed">{{$datas->title}}</h1>
<div class="inerbanimg"><img src="{{URL::asset('public/images/slider/'.$datas->banner)}}"alt="bannerimg" title="bannerimg"></div>

</div>
</section>

<!-- @if(Session::has('message'))

      <div class="callout callout-warning">
          <h4>{{ Session::get('message') }}</h4>
      </div>

@endif -->

@endforeach

<button onclick="mesagemodal()"
 class="button" id="messageBtn" style="display: none">Open Modal</button>


<button onclick="mesagemodal()" class="button" id="messageBtn" style="display: none">Open Modal</button>

@if(Session::has('message'))
<div class="ssn_msg">
<div id="messageModal" class="modal fade">
  <div class="modal-content">
  <div class="mc_clz">

      <span onclick="closeMessageModal()"
      class="close">&times;</span>
      {{Session::get('message')}}
    </div>
  </div>
</div>
</div>

@endif


@foreach($polldet as $poll)

<section class="inner_content pollsec whitebg">
<div class="wid">


<div class="pol1"><img src="{{URL::asset('public/images/slider/'.$poll->pollbanner)}}" alt="slider" title="slider"></div>
<div class="hapyyou inerspcls">
@php
echo $poll->pollcontent;
@endphp


<form method="post" action="{{URL::route('pollsave')}}" id="savepoll">

{{csrf_field()}}

  <p>
    <input type="radio" id="test1" name="pollcheck" value="4" checked>
    <label for="test1">Excellent</label>
  </p>
  <p>
    <input type="radio" id="test2" name="pollcheck" value="3">
    <label for="test2">Good</label>
  </p>
  <p>
    <input type="radio" id="test3" name="pollcheck" value="2">
    <label for="test3">Ok</label>
  </p>
   <p>
    <input type="radio" id="test4" name="pollcheck" value="1">
    <label for="test4">Adverse</label>
  </p>

<button>Vote</button>

<input type="hidden" name="hidpollid" value="{{$poll->pollid}}">

</form>

</div>


</div>
</section>

<section class="grph">
<div class="wid">
<h3 class="cmnheding">Results</h3>
<div class="barChart">
        <div class="barChart__row" data-value="{{$pgdetexcelent}}">
            <span class="barChart__label">Excellent</span>
            <span class="barChart__value">{{$pgdetexcelent}} Votes</span>
            <span class="barChart__bar"><span class="barChart__barFill"></span></span>
        </div>
        <div class="barChart__row" data-value="{{$pgdetgood}}">
            <span class="barChart__label">Good</span>
            <span class="barChart__value">{{$pgdetgood}} Votes</span>
            <span class="barChart__bar"><span class="barChart__barFill"></span></span>
        </div>
        <div class="barChart__row" data-value="{{$pgdetok}}">
            <span class="barChart__label">OK</span>
            <span class="barChart__value">{{$pgdetok}} Votes</span>
            <span class="barChart__bar"><span class="barChart__barFill"></span></span>
        </div>
        <div class="barChart__row" data-value="{{$pgdetbad}}">
            <span class="barChart__label">Bad</span>
            <span class="barChart__value">{{$pgdetbad}} Votes</span>
            <span class="barChart__bar"><span class="barChart__barFill"></span></span>
        </div>

    </div>


</div>
</section>

@endforeach
<script>

$('#messageModal').modal({backdrop:'static', keyboard:false});

  $(document).ready(function() {
  @if(Session::has('message'))
   // var x = "{{Session::get('message')}}";
    $("#messageBtn").trigger('click');
  @else
  $("#messageModal").hide();
  @endif


});

function mesagemodal() {

var modal = document.getElementById('messageModal');


var btn = document.getElementById("messageBtn");

    modal.style.display = "block";

  window.onclick = function(event) {
      if (event.target == modal) {
           modal.style.display = "none";
       }
  }
}

function closeMessageModal() {

    var modal = document.getElementById('messageModal');
location.reload();
    modal.style.display = "none";
}



window.onclick = function(event) {
  var modal = document.getElementsByClassName("modal");

       $('.modal').hide();
      if (event.target == modal) {
           modal.style.display = "none";
       }
  }
</script>
@endsection
