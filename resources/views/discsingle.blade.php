@extends('layouts.app')

@section ('content')

@php
$data = $pagedet['slugData'];
//echo '<pre>';
//print_r($data);
//exit;
@endphp

@foreach($discdet as $ckdet)


<section class="inner_page cookbook" style="background-image:url({{asset('public/frontend/images/banner_bg.jpg')}});">
<div class="wid">
<h3 class="inercmnhed">{{$ckdet->title}}</h3>
<div class="inerbanimg"><img src="{{URL::asset('public/images/slider/'.$ckdet->banner)}}" alt="banner" title="banner"></div>
</div>
</section>

@endforeach


@foreach($data as $datas)
<section class="inner_content cookboksplmain cookbokiner">
<div class="wid">
<div class="Cook">
<div class="sideoverlay">
<div class="cooksec1">
<h1>{{$datas->dtitle}}</h1>


@php
echo $datas->dcontent;
@endphp

</div>

@if($datas->dbanner!='' || $datas->dvideo!='')
<div class="cooksec2">
{{-- <img src="{{asset('public/frontend/images/'.$datas->dimage)}}"> --}}

                @if($datas->dbanner!='' && $datas->dvideo=='')
                   <img src="{{URL::asset('public/images/dishes/'.$datas->dbanner)}}" alt="dishes" title="dishes" />
                @endif
                @if($datas->dvideo!='')
<!-- Trigger/Open The Modal -->
<button id="myBtn" class="btn_disc" onclick="modals()"><img src="{{asset('public/frontend/images/'.$datas->dimage)}}" alt="but" title="button1"></button>

<!-- The Modal -->
<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close" onclick="closemodal()">&times;</span>
    <video width="500" controls>
   <source src="{{asset('public/images/dishes/'.$datas->dvideo)}}" type="video/webm">
                <source src="{{asset('public/images/dishes/'.$datas->dvideo)}}" type="video/mp4">
</video>
  </div>

</div>
@endif






<div class="whtsocial">
<span>Spread the taste</span>

@php
$data = $pagedet['theme'];
@endphp

@foreach($data as $datas)

@php
$tmobile = $datas->tmobile;
$taddress = $datas->taddress;
$tphone = $datas->tphone;
$temail = $datas->temail;
$tfacebook = $datas->tfacebook;
$ttwitter = $datas->ttwitter;
$tyoutube = $datas->tyoutube;
$tinstagram = $datas->tinstagram;
$tcontent = $datas->tcontent;
@endphp

@endforeach
@foreach($discdet as $ckdet)
<ul>
<li><a href="http://www.facebook.com/sharer/sharer.php?u={{URL::to('/'.$ckdet->cmsslug)}}" target="_blank"></a></li>
<li><a href="http://twitter.com/home?status=Fil+in+a+title+{{URL::to('/'.$ckdet->cmsslug)}}" target="_blank"></a></li>
<li><a href="{{$tinstagram}}" target="_blank"></a></li>
<li><a href="{{$tyoutube}}" target="_blank"></a></li>
<li><a href="mailto:?subject=I wanted you to share the Dish&amp;body=Check out this Dish {{URL::to('/'.$ckdet->cmsslug)}}"
   title="Share by Email"></a></li>
</ul>
@endforeach

</div>

</div>
@endif
</div>
</div>

</div>
</section>

@endforeach



<section class="subtrecp">
<div class="wid">
<div class="res1"><img src="{{asset('public/frontend/images/submitres1.png')}}" alt="submitres1" title="submitres1"></div>
<div class="res2"><img src="{{asset('public/frontend/images/submitres2.png')}}" alt="submitres2" title="submitres2"></div>
<div class="res3"><img src="{{asset('public/frontend/images/submitres3.png')}}" alt="submitres3" title="submitres3"></div>


<h3>Submit Your Recipie</h3>
<a href="{{$pagedet['urlSlugs'][23]}}">Submit</a>

</div>
</section>

<script>
function modals(i) {

var modal = document.getElementById('myModal');

var btn = document.getElementById("myBtn");


    modal.style.display = "block";

  window.onclick = function(event) {
      if (event.target == modal) {
           modal.style.display = "none";
       }
  }
}

function closemodal(){
var modal = document.getElementById('myModal');

    modal.style.display = "none";
      $('video').trigger('pause');


}

window.onclick = function(event) {
  var modal = document.getElementsByClassName("modal");

       $('.modal').hide();
      if (event.target == modal) {
           modal.style.display = "none";
       }
  }
</script>



@endsection
