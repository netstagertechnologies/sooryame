@extends('layouts.app')

@section ('content')

@php
$data = $pagedet['slugData'];
//echo '<pre>';
//print_r($cookdet);
//exit;
@endphp

@foreach($cookdet as $ckdet)


<section class="inner_page cookbook" style="background-image:url({{asset('public/frontend/images/banner_bg.jpg')}});">
<div class="wid">
<h3 class="inercmnhed">{{$ckdet->title}}</h3>
<div class="inerbanimg"><img src="{{URL::asset('public/images/slider/'.$ckdet->banner)}}" alt="bannerimg" title="bannerimg"></div>
</div>
</section>

@endforeach


@foreach($data as $pgdata)
@if($pgdata->cmstemplate == 5)
<section class="inner_content cookboksplmain cookbokiner">
<div class="wid">
<div class="Cook">
<div class="sideoverlay">
<div class="cooksec1">
<h1>{{str_replace('-', ' ', urldecode($pgdata->cbrecipename))}}</h1>
@if($pgdata->cbname!='')
<p class="adting">Posted by {{$pgdata->cbname}}. {{$pgdata->cbdate}}</p>
@endif

@php
echo $pgdata->cbrecipecontent;
@endphp

</div>


<div class="cooksec2">
<img src="{{URL::asset('public/images/dishes/'.$pgdata->cbrecipeimage)}}" alt="recipe" title="recipe">
<div class="whtsocial">
<span>Spread the taste</span>
@php
$data = $pagedet['theme'];
@endphp

@foreach($data as $datas)

@php
$tmobile = $datas->tmobile;
$taddress = $datas->taddress;
$tphone = $datas->tphone;
$temail = $datas->temail;
$tfacebook = $datas->tfacebook;
$ttwitter = $datas->ttwitter;
$tyoutube = $datas->tyoutube;
$tinstagram = $datas->tinstagram;
$tcontent = $datas->tcontent;
@endphp

@endforeach
<ul>
@if(count($cook1)=='')
<li><a href=""></a></li>
<li><a href=""></a></li>
@else
<li><a href="http://www.facebook.com/sharer/sharer.php?u={{URL::to('/'.$cook1[0]->cmsslug)}}" target="_blank"></a></li>
<li><a href="http://twitter.com/home?status=Fil+in+a+title+{{URL::to('/'.$cook1[0]->cmsslug)}}" target="_blank"></a></li>
@endif
<li><a href="{{$tinstagram}}" target="_blank"></a></li>
<li><a href="{{$tyoutube}}" target="_blank"></a></li>

</ul>

</div>

</div>
</div>
</div>
@if($pgdata->cbmethodofpreperation != "")
<?php echo $pgdata->cbmethodofpreperation;?>
@endif
</div>

</section>
@endif
@endforeach



<section class="subtrecp">
<div class="wid">
<div class="res1"><img src="{{asset('public/frontend/images/submitres1.png')}}" alt="submitres1" title="submitres1"></div>
<div class="res2"><img src="{{asset('public/frontend/images/submitres2.png')}}" alt="submitres2" title="submitres2"></div>
<div class="res3"><img src="{{asset('public/frontend/images/submitres3.png')}}" alt="submitres3" title="submitres3"></div>


<h3>Submit Your Recipie</h3>
<a href="{{$pagedet['urlSlugs'][23]}}">Submit</a>

</div>
</section>





@endsection
