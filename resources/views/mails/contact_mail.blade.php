<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>SOORYA</title>
<link href="https://fonts.googleapis.com/css?family=Exo:100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,900" rel="stylesheet">
</head>

<body>

<div style="max-width: 620px;
    margin: auto;
    padding: 0;
    border: 2px solid #f9a61c;">
 <div style="margin:30px auto 90px auto; width:100%; height:60px; text-align:center"><a target="_blank" href="{{URL::to('/')}}"><img style="width:200px;" src="{{ $content['logo'] }}"></a></div>
 <div style="width:100%; margin-top:45px;">
  <h1 style="     font-family: 'Exo', sans-serif;
    color: #000000;
    font-size: 21px;
    line-height: 34px;
    font-weight: 400;
    text-align: center;
    padding: 0 20px;">Contact Us Enquiry was received from Soorya Website. Please check the details 
  </h1>
  
  <p style="font-family: 'Exo', sans-serif;
    color: #000;
    text-align: center;
    font-size: 17px;
    line-height: 31px;"><strong>Name :</strong> <span>{{ $content['contactname'] }}</span><br>
<strong>Phone :</strong>  <span>{{ $content['contactphone'] }}</span><br>
<strong>Email :</strong>  <span>{{ $content['contactemail'] }}</span><br>
<strong>Message :</strong>  <span>{{ $content['contactmessage'] }}</span></p><br>
 </div>
    <div style="width:100%; height:60px; background:#ed1c24; display:table;">
     <p style="   font-family: 'Exo', sans-serif;
    color: #fff;
    font-size: 16px;
    text-align: center;
    vertical-align: middle;
    display: table-cell;
    background-color: #068141;">Thank you again | <a target="_blank" style="text-decoration:none; color:#fff;" href="{{URL::to('/')}}">Soorya</a></p>
    </div>
</body>
</html>