@extends('layouts.app')

@section ('content')




@php
$data = $pagedet['slugData'];
//echo '<pre>';
//print_r($data);
//exit; 
@endphp

@foreach($data as $datas)

<section class="inner_page contest whitebg" style="background-image:url({{asset('public/frontend/images/banner_bg.jpg')}});">
<div class="wid">

<h1 class="inercmnhed">{{$datas->title}}</h1>
<div class="inerbanimg"><img src="{{URL::asset('public/images/slider/'.$datas->banner)}}"alt="bannerimg" title="bannerimg"></div>

</div>
</section>

@if(Session::has('message'))

      <div class="callout callout-warning">
          <h4>{{ Session::get('message') }}</h4>
      </div>

@endif

<section class="inner_content contestiner whitebg">
<div class="wid">
@php 
echo $datas->content;
@endphp
</div>
</section>


<section class="sec6 inerspstye cnstfrmsoorya">
<div class="wid">

<h2 class="cmnheding">Personal Details</h2>
<form method="post" action="{{URL::route('savecontest')}}" id="addcontest"  enctype="multipart/form-data">

<input type="hidden" name="_token" value="{{csrf_token()}}">

<ul class="inrfrm">
<li><input type="text" placeholder="Your Name &#42" name="contest_name" id="contest_name"><span class='help-block' id="error1" onmouseover="hide(1);"></span></li>
<li><input type="text" placeholder="Phone &#42" name="contest_phone" id="contest_phone"><span class='help-block' id="error2" onmouseover="hide(2);"></span></li>
<li><input type="text" placeholder="Email &#42" name="contest_email" id="contest_email"><span class='help-block' id="error3" onmouseover="hide(3);"></span>
<div class="gendersp">
<span>Sex :</span>
<p>
<input type="radio" id="test1" name="contest_sex" value="0" checked>
<label for="test1">Male</label>
</p>
<p>
<input type="radio" id="test2" name="contest_sex" value="1">
<label for="test2">female</label>
</p>
</div>

</li>
<li><textarea placeholder="Address" name="contest_address" id="contest_address"></textarea><span class='help-block' id="error4" onmouseover="hide(4);"></span></li>

</ul>

<h2 class="cmnheding">Recipe Details</h2>
<ul class="inrfrm">
<li><input type="text" placeholder="Recipe Name" name="contest_recpname" id="contest_recpname"><span class='help-block' id="error5" onmouseover="hide(5);"></span></li>
<li>
<div class="custom-file-upload">
    <label for="file">Upload Image (Dish)</label> 
    <input type="file" onchange="getImgName(this)" name="contest_recupimage" id="contest_recupimage" multiple />
    <span></span>
</div>
</li>

<li><textarea placeholder="Ingredients" name="contest_ingredients" id="contest_ingredients"></textarea></li>
<li><textarea placeholder="Method of Preparation" name="contest_methprep" id="contest_methprep"></textarea></li>
<li><div class="custom-file-upload">
    <label for="file">Upload Video (Dish Preparing)</label> 
    <input type="file" onchange="getVdoName(this)" name="contest_upvideo" id="contest_upvideo" multiple />
    <span></span>
</div></li>
<li><div class="custom-file-upload">
    <label for="file">Upload Doc (.pdf, .doc, .xls etc)</label> 
    <input type="file" onchange="getDocName(this)" name="contest_updoc" id="contest_updoc" multiple />
    <span></span>
</div></li>
</ul>
<input type="submit" class="cntstsub" value="Submit" onclick="validation_check();return false;">
</form>


</div>
</section>

<section class="winers">
<div class="wid">
<h3 class="cmnheding">Winners</h3>
<ul>

@php
$cnt = 1;
@endphp

@foreach($cnstdet as $value)


@php
if($value->cnstresult==1) $price = '1st Prize';
if($value->cnstresult==2) $price = '2nd Prize';
if($value->cnstresult==3) $price = '3rd Prize';
@endphp

<li>
<span>{{$price}}</span>
<div class="winrimg">
@if($value->cbrecipeprofimage=='')
<img src="{{asset('public/frontend/images/dummyface.jpg')}}"
alt="dummy" title="dummy">
@else
<img src="{{URL::asset('public/images/dishes/'.$datas->cbrecipeprofimage)}}"alt="profile" title="profile">
@endif
</div>
<div class="winername">
{{$value->cbname}}
<small>({{$value->cbrecipename}})</small>
</div>
</li>

@php
$cnt++;
@endphp

@endforeach



</ul>



</div>
</section>

@endforeach

@endsection

<script type="text/javascript" src="{{asset('public/frontend/js/jquery.min.js')}}"></script>

<script type="text/javascript">

function getImgName(e) {

var inp = document.getElementById('contest_recupimage');

var imgname = "";

for (var i = 0; i < inp.files.length; ++i) {

  var name = inp.files.item(i).name;

  imgname += name + ",";
  
}
$(e).next('span').addClass('fl_nm');
$(e).next('span').text(imgname);
console.log(imgname);

}

function getVdoName(e) {

var inp = document.getElementById('contest_upvideo');

var vdoname = "";

for (var i = 0; i < inp.files.length; ++i) {

  var name = inp.files.item(i).name;

  vdoname += name + ",";
  
}
$(e).next('span').addClass('fl_nm');
$(e).next('span').text(vdoname);
console.log(vdoname);

}
		


function getDocName(e) {

var inp = document.getElementById('contest_updoc');

var docname = "";

for (var i = 0; i < inp.files.length; ++i) {

  var name = inp.files.item(i).name;

  docname += name + ",";
  
}
$(e).next('span').addClass('fl_nm');
$(e).next('span').text(docname);
console.log(docname);

}

	function validation_check() {
			
			
			
			var flag=0;
			
			if($('#contest_name').val()=='') {
				$('#error1').show();
				$('#error1').html('Please Enter Your Name');
				goToByScroll("contest_name");
                $("#contest_name").focus();
				flag=1;
			}
			if($('#contest_phone').val()=='') {
				$('#error2').show();
				$('#error2').html('Please Enter Your Phone');
				goToByScroll("contest_phone");
                $("#contest_phone").focus();
				flag=1;
			}
			if($('#contest_email').val()=='') {
				$('#error3').show();
				$('#error3').html('Please Enter Your Email');
				goToByScroll("contest_email");
                $("#contest_email").focus();

				flag=1;
			}
			if($('#contest_address').val()=='') {
				$('#error4').show();
				$('#error4').html('Please Enter Your Address');
				goToByScroll("contest_address");
                $("#contest_address").focus();
				flag=1;
			}
			if($('#contest_recpname').val()=='') {
				$('#error5').show();
				$('#error5').html('Please Enter Recipe Name');
				goToByScroll("contest_recpname");
                $("#contest_recpname").focus();
				flag=1;
			}

			if($("#contest_email").val())
                            {
                                var x = $("#contest_email").val();
                                var atpos = x.indexOf("@");
                                var dotpos = x.lastIndexOf(".");
                                if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) 
                                   {
                                    alert("Not a valid e-mail address");
                                    flag=1;
                                   } 
                                    
                               
                            }
			
			if(flag==1) {
				return false;
			} else {
				$( "#addcontest" ).submit();
				return true;
			}
			
		}

				   function goToByScroll(id, msg) { 
          
                            $("#"+id).parent().find('span').text(msg);              
                            $("#"+id).parent().parent().parent().find('.help-block').removeClass('hide');
                            $("#"+id).parent().parent().parent().addClass('has-error');
                            $("#"+id).focus();
                           
                            // Remove "link" from the ID
                            id = id.replace("link", "");
                            // Scroll
                            // $('html,body').animate({ scrollTop: $("#" + id).offset().top - 100}, 'slow');
                        }
		
		function hide(id) {
			
			var errid = '#error'+id;
			$(errid).hide();
			$(errid).html('');
			
			
		}
	</script>
