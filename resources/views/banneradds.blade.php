<!-- banner 3 -->
<!-- SLIDE  -->
<li data-transition="zoomin" data-slotamount="7" data-masterspeed="1500" data-thumb="homeslider_thumb4.jpg"  data-saveperformance="on"  data-title="Mobile Interaction">
        <div class="tp-caption tp-caption black_thin_34 customin tp-resizeme rs-parallaxlevel-0 sliitem1"
           data-x="128"
           data-y="273"
           data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
           data-speed="1000"
           data-start="2000"
           data-easing="Power4.easeInOut"
           data-elementdelay="0.1"
           data-endelementdelay="0.1"
           data-endspeed="300"
           style="z-index: 3;">
           <div data-easing="Power4.easeInOut"
              data-speed="0.5"
              data-zoomstart="0.75"
              data-zoomend="1">
              <img alt="banner_border" title="banner_border" src="#" data-lazyload="{{asset('public/frontend/images/banner_border.png')}}">
           </div>
        </div>
        <div class="tp-caption large_text sliitem2"
           data-x="0"
           data-y="344"
           data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
           data-speed="1000"
           data-start="2500"
           data-easing="Power4.easeInOut"
           data-splitin="chars"
           data-splitout="none"
           data-elementdelay="0.1"
           data-endelementdelay="0.1"
           data-endspeed="300"
           style="z-index: 3;">
           <div data-easing="Power4.easeInOut"
              data-speed="0.5"
              data-zoomstart="0.75"
              data-zoomend="1">
              Ethnic Indian Taste
           </div>
        </div>
        <div class="tp-caption medium_text sliitem3"
           data-x="-17"
           data-y="406"
           data-speed="1000"
           data-start="3000"
           data-easing="Power3.easeInOut"
           data-splitin="none"
           data-splitout="none"
           data-elementdelay="0.1"
           data-endelementdelay="0.1"
           data-endspeed="300"
           style="z-index: 6; max-width: auto; max-height: auto; white-space: nowrap;"><h1>Traditional Stone Grinded Idli/Dosa/Uttapam Batter Mix</div>
        <div class="tp-caption tp-resizeme sliitem4"
           data-x="160"
           data-y="497"
           data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
           data-speed="1000"
           data-start="3500"
           data-easing="Power3.easeInOut"
           data-splitin="none"
           data-splitout="none"
           data-elementdelay="0.1"
           data-endelementdelay="0.1"
           data-endspeed="600"
           style="z-index: 8; max-width: auto; max-height: auto; white-space: nowrap;"><a href="{{$pagedet['urlSlugs'][7]}}">Know More</a></div>
        <div class="tp-caption sfr fadeout rs-parallaxlevel-5 sliitem5"
           data-x="right" data-hoffset="-70"
           data-y="top" data-voffset="195"
           data-speed="1000"
           data-start="1500"
           data-easing="Power1.easeInOut"
           data-elementdelay="0.1"
           data-endelementdelay="0.1"
           data-endspeed="300"
           style="z-index: 2;"><img alt="banner_soorya_b1" src="#"  title="banner_soorya_b1" data-lazyload="{{asset('public/frontend/images/banner_soorya_b.png')}}"></div>
     
     </li>
     <!-- banner 3 ends -->
     <!-- banner 4 -->
     <li data-transition="zoomin" data-slotamount="7" data-masterspeed="1500" data-thumb="homeslider_thumb4.jpg"  data-saveperformance="on"  data-title="Mobile Interaction">
        <div class="tp-caption tp-caption black_thin_34 customin tp-resizeme rs-parallaxlevel-0 sliitem1"
           data-x="128"
           data-y="273"
           data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
           data-speed="1000"
           data-start="2000"
           data-easing="Power4.easeInOut"
           data-elementdelay="0.1"
           data-endelementdelay="0.1"
           data-endspeed="300"
           style="z-index: 3;">
           <div data-easing="Power4.easeInOut"
              data-speed="0.5"
              data-zoomstart="0.75"
              data-zoomend="1">
              <img alt="banner_border1" title="banner_border1" src="#" data-lazyload="{{asset('public/frontend/images/banner_border.png')}}">
           </div>
        </div>
        <div class="tp-caption large_text sliitem2"
           data-x="0"
           data-y="344"
           data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
           data-speed="1000"
           data-start="2500"
           data-easing="Power4.easeInOut"
           data-splitin="chars"
           data-splitout="none"
           data-elementdelay="0.1"
           data-endelementdelay="0.1"
           data-endspeed="300"
           style="z-index: 3;">
           <div data-easing="Power4.easeInOut"
              data-speed="0.5"
              data-zoomstart="0.75"
              data-zoomend="1">
              Ethnic Indian Taste
           </div>
        </div>
        <div class="tp-caption medium_text sliitem3"
           data-x="-17"
           data-y="406"
           data-speed="1000"
           data-start="3000"
           data-easing="Power3.easeInOut"
           data-splitin="none"
           data-splitout="none"
           data-elementdelay="0.1"
           data-endelementdelay="0.1"
           data-endspeed="300"
           style="z-index: 6; max-width: auto; max-height: auto; white-space: nowrap;">Traditional Stone Grinded Idli/Dosa/Uttapam Batter Mix</div>
        <div class="tp-caption tp-resizeme sliitem4"
           data-x="160"
           data-y="497"
           data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
           data-speed="1000"
           data-start="3500"
           data-easing="Power3.easeInOut"
           data-splitin="none"
           data-splitout="none"
           data-elementdelay="0.1"
           data-endelementdelay="0.1"
           data-endspeed="600"
           style="z-index: 8; max-width: auto; max-height: auto; white-space: nowrap;"><a href="{{$pagedet['urlSlugs'][7]}}">Know More</a></div>
        <div class="tp-caption sfr fadeout rs-parallaxlevel-5 sliitem5"
           data-x="right" data-hoffset="-70"
           data-y="top" data-voffset="195"
           data-speed="1000"
           data-start="1500"
           data-easing="Power1.easeInOut"
           data-elementdelay="0.1"
           data-endelementdelay="0.1"
           data-endspeed="300"
           style="z-index: 2;"><img alt="banner_soorya_c" title="banner_soorya_c" src="#" data-lazyload="{{asset('public/frontend/images/banner_soorya_c.png')}}"></div>
     </li>
     <!-- banner 4 ends -->

     <!-- bANNER 5 JP ADDED -->

     <li data-transition="zoomin" data-slotamount="7" data-masterspeed="1500" data-thumb="homeslider_thumb4.jpg"  data-saveperformance="on"  data-title="Mobile Interaction">
            <div class="tp-caption tp-caption black_thin_34 customin tp-resizeme rs-parallaxlevel-0 sliitem1"
               data-x="128"
               data-y="273"
               data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
               data-speed="1000"
               data-start="2000"
               data-easing="Power4.easeInOut"
               data-elementdelay="0.1"
               data-endelementdelay="0.1"
               data-endspeed="300"
               style="z-index: 3;">
               <div data-easing="Power4.easeInOut"
                  data-speed="0.5"
                  data-zoomstart="0.75"
                  data-zoomend="1">
                  <img alt="banner_border" title="banner_border" src="#" data-lazyload="{{asset('public/frontend/images/banner_border.png')}}">
               </div>
            </div>
            <div class="tp-caption large_text sliitem2"
               data-x="0"
               data-y="344"
               data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
               data-speed="1000"
               data-start="2500"
               data-easing="Power4.easeInOut"
               data-splitin="chars"
               data-splitout="none"
               data-elementdelay="0.1"
               data-endelementdelay="0.1"
               data-endspeed="300"
               style="z-index: 3;">
               <div data-easing="Power4.easeInOut"
                  data-speed="0.5"
                  data-zoomstart="0.75"
                  data-zoomend="1">
                  Ethnic Indian Taste
               </div>
            </div>
            <div class="tp-caption medium_text sliitem3"
               data-x="-17"
               data-y="406"
               data-speed="1000"
               data-start="3000"
               data-easing="Power3.easeInOut"
               data-splitin="none"
               data-splitout="none"
               data-elementdelay="0.1"
               data-endelementdelay="0.1"
               data-endspeed="300"
               style="z-index: 6; max-width: auto; max-height: auto; white-space: nowrap;">Traditional Stone Grinded Idli/Dosa/Uttapam Batter Mix</div>
            <div class="tp-caption tp-resizeme sliitem4"
               data-x="160"
               data-y="497"
               data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
               data-speed="1000"
               data-start="3500"
               data-easing="Power3.easeInOut"
               data-splitin="none"
               data-splitout="none"
               data-elementdelay="0.1"
               data-endelementdelay="0.1"
               data-endspeed="600"
               style="z-index: 8; max-width: auto; max-height: auto; white-space: nowrap;"><a href="{{$pagedet['urlSlugs'][7]}}">Know More</a></div>
            <div class="tp-caption sfr fadeout rs-parallaxlevel-5 sliitem5"
               data-x="right" data-hoffset="-70"
               data-y="top" data-voffset="195"
               data-speed="1000"
               data-start="1500"
               data-easing="Power1.easeInOut"
               data-elementdelay="0.1"
               data-endelementdelay="0.1"
               data-endspeed="300"
               style="z-index: 2;"><img alt="banner_soorya_d" title="banner_soorya_d" src="#" data-lazyload="{{asset('public/frontend/images/banner_soorya_d.png')}}"></div>
         </li>
         <!-- JP Added -->
