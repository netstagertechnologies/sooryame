@extends('layouts.app')

@section ('content')

@php
$data = $pagedet['slugData'];
@endphp

@foreach($pg_desc as $page)
@endforeach
<section class="inner_page contact whitebg" style="background-image:url({{asset('public/frontend/images/banner_bg.jpg')}});">
<div class="wid">
<h1 class="inercmnhed">{{$page->title}}</h1>
<div class="inerbanimg"><img src="{{URL::asset('public/images/slider/'.$page->banner)}}" alt="banner1" title="banner1"></div>
</div>
</section>
<section class="inner_content knowcont">
<div class="wid">
 <p>@php echo $page->content; @endphp</p>

</div>
</section>


@endsection
