@extends('admin.layout.app')

@section ('content')



<div class="page-content">
<h3 class="page-title">Dishes</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<a href="{{URL::route('list_contest')}}">List</a>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
			 <!--<div class="page-toolbar">
					<div class="btn-group pull-right">
					<a href="#">
						<button type="button" class="btn btn-fit-height grey-salt">
						List All <i class="fa fa-list"></i>
						</button>
					</a>
					</div>
				</div>-->
			</div>
 
				<div class="row">
				<div class="col-md-12 ">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-pencil"></i> Edit Page
							</div>
							
						</div>
						<div class="portlet-body form">
					

						<form method="post" action="{{URL::route('editsavecontest')}}" id="adddishes"  enctype="multipart/form-data">
                        
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
				              
				                <div class="form-body">
                                
                                
                                @foreach($data as $datas)
                				 	
   	                				<div class="form-group">
										<label class="col-md-3 control-label">Contestant Name *</label> 
										<div class="col-md-8">
											<div class="input-icon right">
 											  <input type="text"  name="contest_name" class="form-control" id="contest_name" value="{{$datas->cbname}}" readonly="readonly"/>
									 			<span class='help-block' id="error01" onmouseover="hide(01);"></span>
 											</div>
										</div>
									</div>
                                    
                                    <div class="form-group">
										<label class="col-md-3 control-label">Contestant Email *</label> 
										<div class="col-md-8">
											<div class="input-icon right">
 											  <input type="text"  name="contest_email" class="form-control" id="contest_email" value="{{$datas->cbemail}}" readonly="readonly"/>
									 			<span class='help-block' id="error02" onmouseover="hide(02);"></span>
 											</div>
										</div>
									</div>
                                    
                                    <div class="form-group">
										<label class="col-md-3 control-label">Contestant Phone *</label> 
										<div class="col-md-8">
											<div class="input-icon right">
 											  <input type="text"  name="contest_phone" class="form-control" id="contest_phone" value="{{$datas->cbphone}}" readonly="readonly"/>
									 			<span class='help-block' id="error03" onmouseover="hide(03);"></span>
 											</div>
										</div>
									</div>
                                    
                                    <div class="form-group">
										<label class="col-md-3 control-label">Contestant Address *</label> 
										<div class="col-md-8">
											<div class="input-icon right">
 											  <textarea name="contest_address" id="contest_address" readonly="readonly">{{$datas->cbaddress}}</textarea>
									 			<span class='help-block' id="error04" onmouseover="hide(04);"></span>
 											</div>
										</div>
									</div>
                                    
                                    <div class="form-group">
										<label class="col-md-3 control-label">Contestant Sex *</label> 
										<div class="col-md-8">
											<div class="input-icon right">
                                            @php
                                            	if($datas->cbsex==0) $sex = 'male';
                                                if($datas->cbsex==1) $sex = 'Female';
                                            @endphp
 											  <input type="text"  name="contest_sex" class="form-control" id="contest_sex" value="{{$sex}}" readonly="readonly"/>
									 			<span class='help-block' id="error04" onmouseover="hide(04);"></span>
 											</div>
										</div>
									</div>
                                    
                                    
 									<div class="form-group">
										<label class="col-md-3 control-label">Recipie Name *</label> 
										<div class="col-md-8">
											<div class="input-icon right">
 											  <input type="text"  name="dish_name" class="form-control" id="dish_name" value="{{$datas->cbrecipename}}"/>
									 			<span class='help-block' id="error2" onmouseover="hide(2);"></span>
 											</div>
										</div>
									</div>			
	 			
	
 									<div class="form-group">
										<label class="col-md-3 control-label">Ingredients </label> 
										<div class="col-md-8">
											<div class="input-icon right">
											    <textarea  name="dish_ingrdients" id="dish_ingrdients" rows="20" cols="70">{{$datas->cbrecipeincrediant}}</textarea>	
 											   <span class='help-block' id="error3" onmouseover="hide(3);"></span>
  											</div>
										</div>
									</div>			
	 
 									<div class="form-group">
										<label class="col-md-3 control-label">Method of Preparation </label> 
										<div class="col-md-8">
											<div class="input-icon right">	
                                               <textarea  name="dish_preparation" id="dish_preparation" rows="20" cols="70">{{$datas->cbmethodofpreperation}}</textarea>	
									 			<span class='help-block' id="error4" onmouseover="hide(4);"></span>
 											</div>
										</div>
									</div>	
                                    
                                
                                    
                                    @if($datas->cbrecipeimage!='')
                                    <div class="form-group">
										<label class="col-md-3 control-label">Dish Image</label> 
										<div class="col-md-8">
											<div class="input-icon right">	
                                               <!--<input type="file"  name="dish_image" id="dish_image" />
                                               <input type="hidden" name="dishimage" id="dishimage" value="{{$datas->cbrecipeimage}}">-->
                                               <img src="{{URL::asset('public/images/dishes/'.$datas->cbrecipeimage)}}" width="50" />
									 			<span class='help-block' id="error4" onmouseover="hide(4);"></span>
 											</div>
										</div>
									</div>	
                                    @endif
                                    
                                     @if($datas->cbrecipevideo!='')
                                    <div class="form-group">
										<label class="col-md-3 control-label">Dish Video</label> 
										<div class="col-md-8">
											<div class="input-icon right">	
                                               <!--<input type="file"  name="dish_video" id="dish_video" />
                                               <input type="hidden" name="dishvideo" id="dishvideo" value="{{$datas->cbrecipevideo}}">-->
                                               <video width="320" height="240" controls>
                                                      <source src="{{URL::asset('public/images/dishes/'.$datas->cbrecipevideo)}}" type="video/mp4">
                                                      <source src="{{URL::asset('public/images/dishes/'.$datas->cbrecipevideo)}}" type="video/ogg">
                                                      Your browser does not support the video tag.
                                                </video>
									 			<span class='help-block' id="error4" onmouseover="hide(4);"></span>
 											</div>
										</div>
									</div>	
                                    @endif
                                    
                                    @if($datas->cbrecipedoc!='')
                                    <div class="form-group">
										<label class="col-md-3 control-label">Dish Doc</label> 
										<div class="col-md-8">
											<div class="input-icon right">	
                                               <!--<input type="file"  name="dish_video" id="dish_video" />
                                               <input type="hidden" name="dishvideo" id="dishvideo" value="{{$datas->cbrecipevideo}}">-->
                                               		<a href="{{URL::asset('images/dishes/'.$datas->cbrecipedoc)}}">Download Doc</a>                                               
									 			<span class='help-block' id="error4" onmouseover="hide(4);"></span>
 											</div>
										</div>
									</div>
                                    @endif
                                    
                                    <div class="form-group">
										<label class="col-md-3 control-label">Select Winner</label> 
										<div class="col-md-8">
											<div class="input-icon right">	
                                              <select  name="winner_sel" id="winner_sel" />
									 		   		<option value="0" @if($datas->cnstresult == 0) selected="selected" @endif>Select</option>
                                                    <option value="1" @if($datas->cnstresult == 1) selected="selected" @endif>First</option>
                                                    <option value="2" @if($datas->cnstresult == 2) selected="selected" @endif>Second</option>
                                                    <option value="3" @if($datas->cnstresult == 3) selected="selected" @endif>Third</option>
                                               </select>
                                                <span class='help-block' id="error4" onmouseover="hide(4);"></span>
 											</div>
										</div>
									</div>	
                                    				

 								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-4 col-md-8">
											<button type="reset" class="btn default">Cancel</button>
											<input type="hidden" name="dishid" id="dishid" value="{{$datas->cbid}}">
											<button type="submit" class="btn blue" onclick="validation_check();return false;">Submit</button>
										</div>
									</div>
								</div>
                                
              				 @endforeach
                             
					</div>
					</form>
				
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
		
	
				</div>
			
			</div>
		</div>



@endsection





<script type="text/javascript">
	
		function validation_check() {
			
			
			
			var flag=0;
			
			if($('#dish_name').val()=='') {
				$('#error2').html('Please Enter Page Title');
				flag=1;
			}
			
			if(flag==1) {
				return false;
			} else {
				$( "#adddishes" ).submit();
				return true;
			}
			
		}
		
		function hide(id) {
			
			var errid = '#error'+id;
			$(errid).html('');
			
			
		}
	</script>
