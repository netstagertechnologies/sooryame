@extends('admin.layout.app')

@section ('content')



<div class="page-content">
<h3 class="page-title">Pages</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<a href="{{URL::route('list_banner')}}">List</a>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
		
			</div>
 
				<div class="row">
				<div class="col-md-12 ">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-pencil"></i> Edit Banner
							</div>
							
						</div>
						<div class="portlet-body form">
					

						<form method="post" action="{{route('editsavebanner')}}" id="addbanner"  enctype="multipart/form-data">
                        
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
				              
				                <div class="form-body">
                				 
								 
								 @foreach($data as $datas)
								 
   									<div class="form-group">
  										<label class="col-md-3 control-label">Banner Name *</label> 
  										<div class="col-md-8">
  											<div class="input-icon right">
                                               <input type="text"  name="banner_name" class="form-control" id="banner_name" value="{{$datas->sname}}"/>	
  									 			<span class='help-block' id="error1" onmouseover="hide(1);"></span>
   											</div>
  										</div>
  									</div>			
   	                
 									<div class="form-group">
										<label class="col-md-3 control-label">Banner Title *</label> 
										<div class="col-md-8">
											<div class="input-icon right">
 											  <textarea  name="banner_title" id="banner_title">{{$datas->stitle}}</textarea>	
									 			<span class='help-block' id="error2" onmouseover="hide(2);"></span>
 											</div>
										</div>
									</div>			
	 			
	
 									<div class="form-group">
										<label class="col-md-3 control-label">Banner Link *</label> 
										<div class="col-md-8">
											<div class="input-icon right">
                                               <input type="text"  name="banner_link" class="form-control" id="banner_link" value="{{$datas->slink}}"/>
 											   <span class='help-block' id="error3" onmouseover="hide(3);"></span>
  											</div>
										</div>
									</div>			
	 
 									<div class="form-group">
										<label class="col-md-3 control-label">Banner Image *</label> 
										<div class="col-md-8">
											<div class="input-icon right">	
                                               <input type="file"  name="banner_image" id="banner_image" />
											   <img src="{{URL::asset('public/images/slider/'.$datas->simage)}}" width="50">
											   <input type="hidden" name="slideriimage" id="slideriimage" value="{{$datas->simage}}">
									 			<span class='help-block' id="error4" onmouseover="hide(4);"></span>
 											</div>
										</div>
									</div>					

 								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-4 col-md-8">
											<button type="reset" class="btn default">Cancel</button>
											<input type="hidden" name="sliderid" id="sliderid" value="{{$datas->sid}}">
											<button type="submit" class="btn blue" onclick="validation_check();return false;">Submit</button>
										</div>
									</div>
								</div>
								
							 @endforeach
					</div>
					</form>
				
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
		
	
				</div>
			
			</div>
		</div>



@endsection





<script type="text/javascript">
	
		function validation_check() {
			
			var flag=0;
			if($('#banner_name').val()=='') {
				$('#error1').html('Please Enter Banner Name');
				flag=1;
			}
			if($('#banner_title').val()=='') {
				$('#error2').html('Please Enter Banner Title');
				flag=1;
			}
			if($('#banner_link').val()=='') {
				$('#error3').html('Please Enter Banner Link');
				flag=1;
			}
			/*if($('#banner_image').val()=='') {
				$('#error4').html('Please Upload Banner Image');
				flag=1;
			}*/
			
			if(flag==1) {
				return false;
			} else {
				$( "#addbanner" ).submit();
				return true;
			}
			
		}
		
		function hide(id) {
			
			var errid = '#error'+id;
			$(errid).html('');
			
			
		}
	</script>

