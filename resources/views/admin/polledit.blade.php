@extends('admin.layout.app')

@section ('content')



<div class="page-content">
<h3 class="page-title">Pages</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<a href="{{URL::route('list_page')}}">List</a>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
			 <!--<div class="page-toolbar">
					<div class="btn-group pull-right">
					<a href="#">
						<button type="button" class="btn btn-fit-height grey-salt">
						List All <i class="fa fa-list"></i>
						</button>
					</a>
					</div>
				</div>-->
			</div>
 
				<div class="row">
				<div class="col-md-12 ">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-pencil"></i> Edit Page
							</div>
							
						</div>
						<div class="portlet-body form">
					

						<form method="post" action="{{URL::route('editsavepoll')}}" id="addpage"  enctype="multipart/form-data">
                        
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
				              
				                <div class="form-body">
                				 
								 
								 @foreach($data as $datas)
								 			
   	                
 									<div class="form-group">
										<label class="col-md-3 control-label">Page Title *</label> 
										<div class="col-md-8">
											<div class="input-icon right">
 											  <input type="text"  name="page_title" class="form-control" id="page_title" value="{{$datas->polltitle}}"/>
									 			<span class='help-block' id="error2" onmouseover="hide(2);"></span>
 											</div>
										</div>
									</div>			
	 			
	
 									<div class="form-group">
										<label class="col-md-3 control-label">Page Content *</label> 
										<div class="col-md-8">
											<div class="input-icon right">
											   <textarea  name="page_cnt" id="page_cnt" rows="20" cols="70">{{$datas->pollcontent}}</textarea>
 											   <span class='help-block' id="error3" onmouseover="hide(3);"></span>
  											</div>
										</div>
									</div>			
	 
 									<div class="form-group">
										<label class="col-md-3 control-label">Banner Image *</label> 
										<div class="col-md-8">
											<div class="input-icon right">	
                                               <input type="file"  name="banner_image" id="banner_image" />
											   <img src="{{URL::asset('images/slider/'.$datas->pollbanner)}}" width="50">
											   <input type="hidden" name="bannerimage" id="bannerimage" value="{{$datas->pollbanner}}">
									 			<span class='help-block' id="error4" onmouseover="hide(4);"></span>
 											</div>
										</div>
									</div>	
                                    				

 								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-4 col-md-8">
											<button type="reset" class="btn default">Cancel</button>
											<input type="hidden" name="pageid" id="pageid" value="{{$datas->pollid}}">
											<button type="submit" class="btn blue" onclick="validation_check();return false;">Submit</button>
										</div>
									</div>
								</div>
								
							 @endforeach
					</div>
					</form>
				
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
		
	
				</div>
			
			</div>
		</div>



@endsection





<script type="text/javascript">
	
		function validation_check() {
			
			var flag=0;
			
			if($('#page_title').val()=='') {
				$('#error2').html('Please Enter Banner Title');
				flag=1;
			}
			if($('#page_cnt').val()=='') {
				$('#error3').html('Please Enter Banner Link');
				flag=1;
			}
			/*if($('#banner_image').val()=='') {
				$('#error4').html('Please Upload Banner Image');
				flag=1;
			}*/
			
			if(flag==1) {
				return false;
			} else {
				$( "#addpage" ).submit();
				return true;
			}
			
		}
		
		function hide(id) {
			
			var errid = '#error'+id;
			$(errid).html('');
			
			
		}
	</script>

