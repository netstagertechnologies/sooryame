@extends('admin.layout.app')
@section ('content')

<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE HEADER-->
				<h3 class="page-title">
			Pages
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="{{URL::route('list_poll')}}">Pages</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<!--<li>
						<a href="{{URL::route('add_poll')}}">Add Pages</a>
					</li>-->
				</ul>
			</div>
 
  			
			<?php
					$msg = ''; $msgcls = 'display-hide';

					if(isset($_GET['message'])){
 
						switch($_GET['message']){
							
							case 'Add-SUCCESS'	: $msg = 'Data Added Successully !';  $msgcls = 'note-success';break;
							case 'Edit-SUCCESS'	: $msg = 'Data Edited Successfully !';$msgcls = 'note-success';break;
							case 'ERROR'		: $msg = 'Failed to Update. Please try again later !'; $msgcls = 'note-warning';
							                      break;
							default             : $msg = 'No Changes Done.'; $msgcls = 'note-info'; break;
						} 
					}
						?>
			<div class="note <?php echo $msgcls;?>">
				<p>
					<?php echo $msg;?>	
				</p>
			</div>


			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN SAMPLE TABLE PORTLET-->
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-cogs"></i>Listings
							</div>
							
						</div>
						<div class="portlet-body flip-scroll">
							<table class="table table-bordered table-striped table-condensed flip-content">
							<thead class="flip-content">
							<tr>
								<th class="numewric">
									 No.
								</th>
								<th>
									Title
								</th>
								<th>
								   	Content
 								<th>
									 Image
								</th>
                                <th>
									 Action
								</th>
								<th>
									Manage Votes
								</th>
							</tr>
							</thead>
							<tbody>
							<?php $i = 0;?>
							
		              	@foreach($data as $datas) <?php $i++;?>
							<tr>
								<td>
									 {{$i}}
								</td>
								<td>
									 {{$datas->polltitle}}
								</td>
								<td>
                                   @php
                                        $content = $datas->pollcontent;
                                        $stripcont = substr($content,0,200);
                                        echo strip_tags($stripcont).'...';
                                    @endphp
								</td>
								<td>
								   <img src="{{URL::asset('images/slider/'.$datas->pollbanner)}}" width="50" />
								</td>
 								<td>
 										
								<a href="{{URL::route('editpoll',Crypt::encrypt($datas->pollid))}}">
								   <i class="fa fa-pencil-square-o"  title="Edit"></i>
								</a>
								<!--<a title="Delete" href="{{URL::route('delpoll',Crypt::encrypt($datas->pollid))}}"  onclick="return confirm('Are you sure you want to delete?')"><i class="fa fa-fw fa-trash-o"></i></a>-->

								</td>
								<td>
								<a href="{{URL::route('managepoll',Crypt::encrypt($datas->pollid))}}">
								   Click Here</i>
								</a>								
								</td>
 							</tr>
                          @endforeach
							</tbody>
							</table>
					
						</div>
					</div>
					<!-- END SAMPLE TABLE PORTLET-->
				
 				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
        
@endsection



