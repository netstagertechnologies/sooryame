@extends('admin.layout.app')

@section ('content')



<div class="page-content">
<h3 class="page-title">Discoveries</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<a href="{{URL::route('list_disc')}}">List</a>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
			 <!--<div class="page-toolbar">
					<div class="btn-group pull-right">
					<a href="#">
						<button type="button" class="btn btn-fit-height grey-salt">
						List All <i class="fa fa-list"></i>
						</button>
					</a>
					</div>
				</div>-->
			</div>
 
				<div class="row">
				<div class="col-md-12 ">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-pencil"></i> Add Discoveries
							</div>
							
						</div>
						<div class="portlet-body form">
					

						<form method="post" action="{{URL::route('savedisc')}}" id="adddisc" accept-charset="UTF-8" enctype="multipart/form-data">
                        
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
				              
				                <div class="form-body">
                				 	
   	                
 									<div class="form-group">
										<label class="col-md-3 control-label">Discoveries Title *</label> 
										<div class="col-md-8">
											<div class="input-icon right">
 											  <input type="text"  name="disc_title" class="form-control" id="disc_title"/>
									 			<span class='help-block' id="error2" onmouseover="hide(2);"></span>
 											</div>
										</div>
									</div>			
	 			
	
 									<div class="form-group">
										<label class="col-md-3 control-label">Discoveries Content *</label> 
										<div class="col-md-8">
											<div class="input-icon right">
											    <textarea  name="disc_cnt" id="disc_cnt" rows="20" cols="70"></textarea>	
 											   <span class='help-block' id="error3" onmouseover="hide(3);"></span>
  											</div>
										</div>
									</div>			
	 
 									<div class="form-group">
										<label class="col-md-3 control-label">Discoveries Image</label> 
										<div class="col-md-8">
											<div class="input-icon right">	
                                               <input type="file"  name="disc_image" id="disc_image" />
									 			<span class='help-block' id="error4" onmouseover="hide(4);"></span>
 											</div>
										</div>
									</div>	
                                    
                                    <div class="form-group">
										<label class="col-md-3 control-label">Discoveries Video </label> 
										<div class="col-md-8">
											<div class="input-icon right">	
                                               <input type="file"  name="disc_video" id="disc_video" />
									 			<span class='help-block' id="error4" onmouseover="hide(4);"></span>
 											</div>
										</div>
									</div>	
                                    
                                    <div class="form-group">
										<label class="col-md-3 control-label">Select Template</label> 
										<div class="col-md-8">
											<div class="input-icon right">	
                                               <select  name="template_sel" id="template_sel" />
									 		   		@foreach($data as $datas)
                                                    	<option value="{{$datas->tcmsid}}">{{$datas->tcmsname}}</option>
                                                    @endforeach
                                               </select>
                                                <span class='help-block' id="error4" onmouseover="hide(4);"></span>
 											</div>
										</div>
									</div>	
									<div class="form-group">
										<label class="col-md-3 control-label">Meta Title </label> 
										<div class="col-md-8">
											<div class="input-icon right">
 											  <input type="text"  name="meta_title" class="form-control" id="meta_title"/>
									 			<span class='help-block' id="error2" onmouseover="hide(2);"></span>
 											</div>
										</div>
									</div>		
									<div class="form-group">
										<label class="col-md-3 control-label">Meta keyword </label> 
										<div class="col-md-8">
											<div class="input-icon right">
 											  <input type="text"  name="meta_keyword" class="form-control" id="meta_keyword"/>
									 			<span class='help-block' id="error2" onmouseover="hide(2);"></span>
 											</div>
										</div>
									</div>		
									<div class="form-group">
										<label class="col-md-3 control-label">Meta Description</label> 
										<div class="col-md-8">
											<div class="input-icon right">
												<textarea  name="meta_desc" id="meta_desc" rows="3" cols="70"></textarea>	
									 			<span class='help-block' id="error2" onmouseover="hide(2);"></span>
 											</div>
										</div>
									</div>							

 								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-4 col-md-8">
											<button type="reset" class="btn default">Cancel</button>
											<button type="submit" class="btn blue" onclick="validation_check();return false;">Submit</button>
										</div>
									</div>
								</div>
              
					</div>
					</form>
				
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
		
	
				</div>
			
			</div>
		</div>



@endsection





<script type="text/javascript">
	
		function validation_check() {
			
			var flag=0;
			
			if($('#disc_title').val()=='') {
				$('#error2').html('Please Enter Discoveries Title');
				flag=1;
			}
			if($('#disc_cnt').val()=='') {
				$('#error3').html('Please Enter Discoveries Content');
				flag=1;
			}
			
			
			if(flag==1) {
				return false;
			} else {
				$( "#adddisc" ).submit();
				return true;
			}
			
		}
		
		function hide(id) {
			
			var errid = '#error'+id;
			$(errid).html('');
			
			
		}
	</script>

