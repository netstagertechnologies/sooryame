@extends('admin.layout.app')

@section ('content')



<div class="page-content">
<h3 class="page-title">Dishes</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<a href="{{URL::route('list_dishes')}}">List</a>
						<i class="fa fa-angle-right"></i>
					</li>
				</ul>
		
			</div>
 
				<div class="row">
				<div class="col-md-12 ">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-pencil"></i> Edit Page
							</div>
							
						</div>
						<div class="portlet-body form">
					

						<form method="post" action="{{URL::route('editsavedish')}}" id="adddishes"  enctype="multipart/form-data">
                        
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
				              
				                <div class="form-body">
                                
                                
                                @foreach($data as $datas)
                				 	
   	                
 									<div class="form-group">
										<label class="col-md-3 control-label">Recipie Name *</label> 
										<div class="col-md-8">
											<div class="input-icon right">
 											  <input type="text"  name="dish_name" class="form-control" id="dish_name" value="{{$datas->cbrecipename}}"/>
									 			<span class='help-block' id="error2" onmouseover="hide(2);"></span>
 											</div>
										</div>
									</div>			
	 			
	
 									<div class="form-group">
										<label class="col-md-3 control-label">Ingredients </label> 
										<div class="col-md-8">
											<div class="input-icon right">
											    <textarea  name="dish_ingrdients" id="dish_ingrdients" rows="20" cols="70">{{$datas->cbrecipeincrediant}}</textarea>	
 											   <span class='help-block' id="error3" onmouseover="hide(3);"></span>
  											</div>
										</div>
									</div>			
	 
 									<div class="form-group">
										<label class="col-md-3 control-label">Method of Preparation </label> 
										<div class="col-md-8">
											<div class="input-icon right">	
                                               <textarea  name="dish_preparation" id="dish_preparation" rows="20" cols="70">{{$datas->cbmethodofpreperation}}</textarea>	
									 			<span class='help-block' id="error4" onmouseover="hide(4);"></span>
 											</div>
										</div>
									</div>	
                                    
                                    <div class="form-group">
										<label class="col-md-3 control-label">Content </label> 
										<div class="col-md-8">
											<div class="input-icon right">	
                                               <textarea  name="dish_content" id="dish_content" rows="20" cols="70">{{$datas->cbrecipecontent}}</textarea>	
									 			<span class='help-block' id="error4" onmouseover="hide(4);"></span>
 											</div>
										</div>
									</div>	
                                    
                                    <div class="form-group">
										<label class="col-md-3 control-label">Dish Image</label> 
										<div class="col-md-8">
											<div class="input-icon right">	
                                               <input type="file"  name="dish_image" id="dish_image" />
                                               <input type="hidden" name="dishimage" id="dishimage" value="{{$datas->cbrecipeimage}}">
                                               @if($datas->cbrecipeimage!='')
                                               <img src="{{URL::asset('public/images/dishes/'.$datas->cbrecipeimage)}}" width="50" />
                                               @endif
									 			<span class='help-block' id="error4" onmouseover="hide(4);"></span>
 											</div>
										</div>
									</div>	
                                    
                                    <div class="form-group">
										<label class="col-md-3 control-label">Dish Video</label> 
										<div class="col-md-8">
											<div class="input-icon right">	
                                               <input type="file"  name="dish_video" id="dish_video" />
                                               <input type="hidden" name="dishvideo" id="dishvideo" value="{{$datas->cbrecipevideo}}">
                                               @if($datas->cbrecipevideo!='')
                                               <video width="320" height="240" controls>
                                                      <source src="{{URL::asset('publicimages/dishes/'.$datas->cbrecipevideo)}}" type="video/mp4">
                                                      <source src="{{URL::asset('publicimages/dishes/'.$datas->cbrecipevideo)}}" type="video/ogg">
                                                      Your browser does not support the video tag.
                                                </video>
                                                @endif
									 			<span class='help-block' id="error4" onmouseover="hide(4);"></span>
 											</div>
										</div>
									</div>	
                                    
                                    <div class="form-group">
										<label class="col-md-3 control-label">Select Template</label> 
										<div class="col-md-8">
											<div class="input-icon right">	
                                              <select  name="template_sel" id="template_sel" />
									 		   		@foreach($tempdata as $tempdatas)
                                                    	<option value="{{$tempdatas->tcmsid}}" 
                                                        @if($datas->cmstemplate==$tempdatas->tcmsid) selected="selected" @endif>{{$tempdatas->tcmsname}}</option>
                                                    @endforeach
                                               </select>
                                                <span class='help-block' id="error4" onmouseover="hide(4);"></span>
 											</div>
										</div>
									</div>	
									<div class="form-group">
										<label class="col-md-3 control-label">Meta Title </label> 
										<div class="col-md-8">
											<div class="input-icon right">
 											  <input type="text"  name="meta_title" value="{{$datas->meta_title}}" class="form-control" id="meta_title"/>
									 			<span class='help-block' id="error2" onmouseover="hide(2);"></span>
 											</div>
										</div>
									</div>		
									<div class="form-group">
										<label class="col-md-3 control-label">Meta keyword </label> 
										<div class="col-md-8">
											<div class="input-icon right">
 											  <input type="text"  name="meta_keyword" value="{{$datas->meta_keyword}}" class="form-control" id="meta_keyword"/>
									 			<span class='help-block' id="error2" onmouseover="hide(2);"></span>
 											</div>
										</div>
									</div>		
									<div class="form-group">
										<label class="col-md-3 control-label">Meta Description</label> 
										<div class="col-md-8">
											<div class="input-icon right">
												<textarea  name="meta_desc" id="meta_desc" rows="3" cols="70">{{$datas->meta_description}}</textarea>	
									 			<span class='help-block' id="error2" onmouseover="hide(2);"></span>
 											</div>
										</div>
									</div>			       				
 								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-4 col-md-8">
											<button type="reset" class="btn default">Cancel</button>
											<input type="hidden" name="dishid" id="dishid" value="{{$datas->cbid}}">
											<button type="submit" class="btn blue" onclick="validation_check();return false;">Submit</button>
										</div>
									</div>
								</div>
                                
              				 @endforeach
                             
					</div>
					</form>
				
						</div>
					</div>
					<!-- END SAMPLE FORM PORTLET-->
		
	
				</div>
			
			</div>
		</div>



@endsection





<script type="text/javascript">
	
		function validation_check() {
			
			
			
			var flag=0;
			
			if($('#dish_name').val()=='') {
				$('#error2').html('Please Enter Page Title');
				flag=1;
			}
			
			if(flag==1) {
				return false;
			} else {
				$( "#adddishes" ).submit();
				return true;
			}
			
		}
		
		function hide(id) {
			
			var errid = '#error'+id;
			$(errid).html('');
			
			
		}
	</script>
