@extends('admin.layout.app')
@section ('title')
{{$title}}
@endsection
@section ('content')

<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE HEADER-->
				<h3 class="page-title">
			Theme Option
			</h3>
			
 
  			
			<?php
					$msg = ''; $msgcls = 'display-hide';

					if(isset($_GET['message'])){
 
						switch($_GET['message']){
							
							case 'Add-SUCCESS'	: $msg = 'Data Added Successully !';  $msgcls = 'note-success';break;
							case 'Edit-SUCCESS'	: $msg = 'Data Edited Successfully !';$msgcls = 'note-success';break;
							case 'ERROR'		: $msg = 'Failed to Update. Please try again later !'; $msgcls = 'note-warning';
							                      break;
							default             : $msg = 'No Changes Done.'; $msgcls = 'note-info'; break;
						} 
					}
						?>
			<div class="note <?php echo $msgcls;?>">
				<p>
					<?php echo $msg;?>	
				</p>
			</div>


			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN SAMPLE TABLE PORTLET-->
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-cogs"></i>Listings
							</div>
							
						</div>
						<div class="portlet-body flip-scroll">
							<table class="table table-bordered table-striped table-condensed flip-content">
							<tbody>
                            
                            
                         @if($data->count() > 0)
		              	@foreach($data as $datas) 
                        	@php
                            	$taddress = $datas->taddress;
                                $tmobile = $datas->tmobile;
                                $tphone = $datas->tphone;
                                $temail = $datas->temail;
                                $tfacebook = $datas->tfacebook;
                                $ttwitter = $datas->ttwitter;
                                $tinstagram = $datas->tinstagram;
                                $tyoutube = $datas->tyoutube;
                                $tcontent = $datas->tcontent;
                                $tid = $datas->tid;
                            @endphp
                         @endforeach
                         @else
                         	@php
                            	$taddress = '';
                                $tmobile = '';
                                $tphone = '';
                                $temail = '';
                                $tfacebook = '';
                                $ttwitter = '';
                                $tinstagram = '';
                                $tyoutube = '';
                                $tcontent = '';
                                $tid = 1;
                            @endphp
                         @endif	   
                            
                            
                       <form method="post" action="{{route('savethoption')}}" id="savethoption"  >
                        
                        <input type="hidden" name="_token" value="{{csrf_token()}}">                      
                        
							<tr>
                            	<td>
									 Address
								</td>
								<div class="pad">
								<td >
									 <textarea id="editor1" name="address" id="address" rows="10" cols="80" >{{$taddress}}</textarea>
								</td>
								</div>
                             </tr>
                             <tr>
                                <td>
									 Mobile
								</td>
								<td>
                               	 	<input type="text"  name="mobile" class="form-control" id="mobile" value="{{$tmobile}}"/>
								</td>
                              </tr>
                              <tr>
                                <td>
									 Phone
								</td>
								<td>
                               	 	<input type="text"  name="phone" class="form-control" id="phone" value="{{$tphone}}"/>
								</td>
                              </tr>
                              <tr>  
                                <td>
									 Email
								</td>
								<td>
                               	 	<input type="text"  name="email" class="form-control" id="email" value="{{$temail}}"/>
								</td>
                              </tr>
                              <tr>  
                                <td>
									 Facebook
								</td>
								<td>
                               	 	<input type="text"  name="facebook" class="form-control" id="facebook" value="{{$tfacebook}}"/>
								</td>
                              </tr>
                              <tr>  
                                <td>
									 Twitter
								</td>
								<td>
                               	 	<input type="text"  name="twitter" class="form-control" id="twitter" value="{{$ttwitter}}"/>
								</td>
                             </tr>
                             <tr>  
                                <td>
									 Instagram
								</td>
								<td>
                               	 	<input type="text"  name="instagram" class="form-control" id="instagram" value="{{$tinstagram}}"/>
								</td>
                             </tr>
                             <tr>  
                                <td>
									 Youtube
								</td>
								<td>
                               	 	<input type="text"  name="youtube" class="form-control" id="youtube" value="{{$tyoutube}}"/>
								</td>
                             </tr>
                             <tr>  
                                <td>
									 Content
								</td>
								<td>
                                    <textarea   style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" name="content" id="content" rows="10" cols="40">{{$tcontent}}</textarea>
								</td>
                             </tr>
                              <tr> 
                              <td>&nbsp; </td>
                              <td>
                                  <input type="hidden" name="optionid" id="optionid" value="{{$tid}}">
                                  <button type="submit" class="btn blue" >Submit</button>
                              </td>
                              </tr>
                          
                          </form>
							</tbody>
							</table>
					
						</div>
					</div>
					<!-- END SAMPLE TABLE PORTLET-->
				
 				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
        
@endsection



