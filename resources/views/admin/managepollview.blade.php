@extends('admin.layout.app')
@section ('content')

<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN PAGE HEADER-->
				<h3 class="page-title">
			Poll Votes
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="{{URL::route('list_poll')}}">Polls</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<!--<li>
						<a href="#">Add Dishes</a>
					</li>-->
				</ul>
			</div>
 
  			
			<?php
					$msg = ''; $msgcls = 'display-hide';

					if(isset($_GET['message'])){
 
						switch($_GET['message']){
							
							case 'Add-SUCCESS'	: $msg = 'Data Added Successully !';  $msgcls = 'note-success';break;
							case 'Edit-SUCCESS'	: $msg = 'Data Edited Successfully !';$msgcls = 'note-success';break;
							case 'ERROR'		: $msg = 'Failed to Update. Please try again later !'; $msgcls = 'note-warning';
							                      break;
							default             : $msg = 'No Changes Done.'; $msgcls = 'note-info'; break;
						} 
					}
						?>
			<div class="note <?php echo $msgcls;?>">
				<p>
					<?php echo $msg;?>	
				</p>
			</div>


			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN SAMPLE TABLE PORTLET-->
					<div class="portlet box blue">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-cogs"></i>Listings
							</div>
							
						</div>
						<div class="portlet-body flip-scroll">
							<table class="table table-bordered table-striped table-condensed flip-content">
							<thead class="flip-content">
							</thead>
							<tbody>

							<tr>
							<form method="POST" action="{{URL::route('save_votes')}}">
							{{csrf_field()}}
							<input type="hidden" name="pollid" value="{{$pollid}}">
								<tr>
									<td> Adverse Count </td>
									<td> {{$badCount}} </td>
									<td><input type="text" name="bad_count" maxlength="3" size="3" placeholder="new">
									</td>
									<td> Total : {{$badTot}}</td>
								</tr>
								<tr>
									<td> Ok Count </td>
									<td>{{$okCount}} </td>
									<td> <input type="text" name="ok_count" maxlength="3" size="3" placeholder="new">
									</td>
									<td> Total : {{$okTot}}</td>
								</tr>
                                <tr>
                                <td> Good Count </td>
                                <td>{{$goodCount}}</td>
                                <td><input type="text" name="good_count" maxlength="3" size="3" placeholder="new"></td>
                                <td> Total : {{$goodTot}}</td>
                                </tr>
                                <tr> 
                                <td> Excellent Count </td>
								<td>
									{{$exCount}}</td>
									<td><input type="text" name="ex_count" maxlength="3" size="3" placeholder="new">
								</td>
								<td> Total : {{$exTot}}</td>
								</tr>
 								<tr>
 								<td>	
								<input type="submit" name="submit">
								</td>
								</tr>
								</form>
 							</tr>
               
							</tbody>
							</table>
					
						</div>
					</div>
					<!-- END SAMPLE TABLE PORTLET-->
				
 				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
        
@endsection



