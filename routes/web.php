<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/clear-cache', function() {
// 	$exitCode = Artisan::call('config:clear');
// 	$exitCode = Artisan::call('cache:clear');
// 	$exitCode = Artisan::call('config:cache');
// 	return 'DONE'; //Return anything
// });

Route::get('/home', 'HomeController@index')->name('home');

Route::get('admin/admin', 'AdminController@index')->name('admin');

// Route::get('admin', function () {
//     return view('admin_template');
// });
Route::get('c_manage','admin\LoginController@login')->name('login');
Route::post('do-login',['as'=>'do.login','uses'=>'admin\LoginController@doLogin']);
Route::post('logout',['as'=>'logout','uses'=>'admin\LoginController@logout']);

Route::group(['middleware'=>'isloggedIn','prefix'=>'admin'],function(){
	Route::get('dashboard',['as'=>'dashboard','uses'=>'admin\LoginController@dashboard']);
    // Auth::routes();
        
	Route::get('themeoption','admin\ThemeController@view_theme')->name('theme_option');
	
	Route::post('savethoption',['as'=>'savethoption','uses' =>'admin\ThemeController@save_option']);
	
	Route::get('bannerlist','admin\BannerController@view_ban')->name('list_banner');
	
	Route::get('addbanner','admin\BannerController@add_ban')->name('add_banner');
	
	Route::post('savebanner',['as'=>'savebanner','uses' =>'admin\BannerController@save_ban']);
	
	Route::post('editsavebanner',['as'=>'editsavebanner','uses' =>'admin\BannerController@editsave_ban']);
	
	Route::get('editbanner/{id}',['as'=>'editbanner','uses' =>'admin\BannerController@edit_ban']);

	Route::get('delbanner/{id}',['as'=>'delbanner','uses' =>'admin\BannerController@del_ban']);
	
	Route::get('pagelist','admin\PageController@view_page')->name('list_page');
	
	Route::get('addpage','admin\PageController@add_page')->name('add_page');
	
	Route::post('savepage',['as'=>'savepage','uses' =>'admin\PageController@save_page']);
	
	Route::post('editsavepage',['as'=>'editsavepage','uses' =>'admin\PageController@editsave_page']);
	
	Route::get('editpage/{id}',['as'=>'editpage','uses' =>'admin\PageController@edit_page']);

	Route::get('delpage/{id}',['as'=>'delpage','uses' =>'admin\PageController@del_page']);
	
	Route::get('dishlist','admin\DishController@view_dishes')->name('list_dishes');
	
	Route::get('adddishes','admin\DishController@add_dish')->name('add_dishes');
	
	Route::post('savedish',['as'=>'savedish','uses' =>'admin\DishController@save_dish']);
	
	Route::get('editdish/{id}',['as'=>'editdish','uses' =>'admin\DishController@edit_dish']);
	
	Route::post('editsavedish',['as'=>'editsavedish','uses' =>'admin\DishController@editsave_dish']);
	
	Route::get('deldish/{id}',['as'=>'deldish','uses' =>'admin\DishController@del_dish']);
	
	Route::get('disclist','admin\DiscController@view_disc')->name('list_disc');
	
	Route::get('adddisc','admin\DiscController@add_disc')->name('add_disc');
	
	Route::post('savedisc',['as'=>'savedisc','uses' =>'admin\DiscController@save_disc']);
	
	Route::post('editsavedisc',['as'=>'editsavedisc','uses' =>'admin\DiscController@editsave_disc']);
	
	Route::get('editdisc/{id}',['as'=>'editdisc','uses' =>'admin\DiscController@edit_disc']);

	Route::get('deldisc/{id}',['as'=>'deldisc','uses' =>'admin\DiscController@del_disc']);
	
	
	Route::get('contestlist','admin\ContestController@view_dishes')->name('list_contest');
	Route::get('editcontest/{id}',['as'=>'editcontest','uses' =>'admin\ContestController@edit_contest']);
	Route::post('editsavecontest',['as'=>'editsavecontest','uses' =>'admin\ContestController@editsave_contest']);
	
	Route::get('editcontest/{id}',['as'=>'editcontest','uses' =>'admin\ContestController@edit_contest']);
	
	Route::get('polllist','admin\PollController@view_poll')->name('list_poll');
	
	Route::get('addpoll','admin\PollController@add_poll')->name('add_poll');
	
	Route::post('savepoll',['as'=>'savepoll','uses' =>'admin\PollController@save_poll']);
	
	Route::post('editsavepoll',['as'=>'editsavepoll','uses' =>'admin\PollController@editsave_poll']);
	
	Route::get('editpoll/{id}',['as'=>'editpoll','uses' =>'admin\PollController@edit_poll']);

	Route::get('delpoll/{id}',['as'=>'delpoll','uses' =>'admin\PollController@del_poll']);
	
	Route::get('managepoll/{id}',['as' => 'managepoll','uses' =>'admin\PollController@manage_poll']);

	Route::post('save_votes',['as' => 'save_votes','uses' => 'admin\PollController@save_votes']);
	
});

Route::get('sitemap','SitemapController@index')->name('sitemap');
Route::post('savecontest',['as'=>'savecontest','uses' =>'SaveController@save_contest']);
Route::post('sentcontact',['as'=>'sentcontact','uses' =>'SaveController@sent_contact']);
Route::post('pollsave',['as'=>'pollsave','uses' =>'SaveController@poll_save']);
Route::post('newssave',['as'=>'newssave','uses' =>'SaveController@news_save']);
Route::get('pagenotfound','IndexController@pagenotfound1')->name('pagenotfound');
Route::any('/{url?}/{id?}',['as'=>'index','uses'=>'IndexController@index']);
// Route::get('/',['as'=>'home','uses'=>'IndexController@home']);




